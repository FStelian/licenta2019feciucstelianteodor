module.exports = (() => {
    const getCookie = (header, cookieToFind) => {
        if (header["cookie"] === undefined) return "";
        let cookies = header["cookie"].split("; ");
        let cookieToReturn = "";

        let regex = new RegExp(`^${cookieToFind}=`)
        cookieToReturn = cookies.find(cookie => regex.test(cookie));

        if (cookieToReturn) {
            cookieToReturn = cookieToReturn.replace(regex, "");
        }

        return cookieToReturn;
    }

    const setCookie = (cookieName, cookieValue) => {
        return {"Set-Cookie": `${cookieName}=${cookieValue}; Path=/`};
    }

    return {
        getCookie,
        setCookie
    }
})();