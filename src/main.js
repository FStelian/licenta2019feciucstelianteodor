var http = require("http");
var router = require("./router");
var files = require("./files/files");
let login = require("./actions/login");
let logout = require("./actions/logout");
let register = require("./actions/register");
let changePassword = require("./actions/changePassword");
let editProfile = require("./actions/editProfile");

let useTicket = require("./actions/useTicket");
let getStatistics = require("./actions/getStatistics");
let contact = require("./actions/contact");
let addAdmin = require("./actions/addAdmin");

let getMapRoute = require("./actions/getMapRoute");
let getQR = require("./actions/getQR");
let getMapRouteById = require("./actions/getMapRouteById");
let getAllMapRoutes = require("./actions/getAllMapRoutes");
let addMapRoute = require("./actions/addMapRoute");
let editMapRoute = require("./actions/editMapRoute");
let addRouteVehicle = require("./actions/addRouteVehicle");
let deleteRouteVehicle = require("./actions/deleteRouteVehicle");
let estimate = require("./actions/estimate");

let buyTickets = require("./actions/buyTickets");
let paymentSuccess = require("./actions/paymentSuccess");
let paymentCancel = require("./actions/paymentCancel");

let vehicleHelper = require("./utils/updateVehicleStation");

var initRoutes = () => {
    router.use(files.staticFiles);

    router.addRoute({
        authed: false,
        method: "POST",
        url: "/users/login",
        fn: login
    });

    router.addRoute({
        authed: true,
        method: "GET",
        url: "/users/logout",
        fn: logout
    });
    
    router.addRoute({
        authed: false,
        method: "POST",
        url: "/users",
        fn: register
    });

    router.addRoute({
        authed: true,
        method: "PUT",
        url: "/users",
        fn: editProfile
    });

    router.addRoute({
        authed: true,
        method: "POST",
        url: "/tickets/use",
        fn: useTicket
    });

    router.addRoute({
        authed: true,
        admin: true,
        method: "GET",
        url: "/statistics",
        fn: getStatistics
    });

    router.addRoute({
        authed: true,
        method: "POST",
        url: "/contact",
        fn: contact
    });

    router.addRoute({
        authed: true,
        method: "POST",
        url: "/changePassword",
        fn: changePassword
    });

    router.addRoute({
        authed: true,
        admin: true,
        method: "POST",
        url: "/route",
        fn: getMapRoute
    });

    router.addRoute({
        authed: true,
        method: "POST",
        url: "/routes/byId",
        fn: getMapRouteById
    });

    router.addRoute({
        authed: true,
        admin: true,
        method: "POST",
        url: "/routes",
        fn: addMapRoute
    });
    
    router.addRoute({
        authed: true,
        admin: true,
        method: "POST",
        url: "/vehicle/routes",
        fn: addRouteVehicle
    });

    router.addRoute({
        authed: true,
        admin: true,
        method: "DELETE",
        url: "/vehicle/routes",
        fn: deleteRouteVehicle
    });

    router.addRoute({
        authed: true,
        admin: true,
        method: "GET",
        url: "/routes",
        fn: getAllMapRoutes
    });

    router.addRoute({
        authed: true,
        admin: true,
        method: "PUT",
        url: "/routes",
        fn: editMapRoute
    });

    router.addRoute({
        authed: true,
        method: "POST",
        url: "/estimate",
        fn: estimate
    });

    router.addRoute({
        authed: true,
        admin: true,
        method: "POST",
        url: "/addAdmin",
        fn: addAdmin
    });
	
		router.addRoute({
        authed: true,
        method: "POST",
        url: "/buyTickets",
        fn: buyTickets
    });
	
	router.addRoute({
        authed: true,
        method: "GET",
        url: "/paymentsuccess",
        fn: paymentSuccess
    });
	
	router.addRoute({
        authed: true,
        method: "GET",
        url: "/paymentcancel",
        fn: paymentCancel
    });

    router.addRoute({
        authed: true,
        method: "GET",
        url: "/qr",
        fn: getQR
    });
};

var server = http.createServer(function(req, res) {
    router.route(req, res);
});

initRoutes();

server.listen(8081, process.env.OPENSHIFT_NODEJS_IP || process.env.IP || '127.0.0.1');
console.log("Listening at http://localhost:8081");

vehicleHelper.updateVehicleStation();
