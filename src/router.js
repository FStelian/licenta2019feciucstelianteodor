module.exports = (() => {
    const database = require("./db/database");
    const cookie = require("./cookie");
	var parser = require('url');
    let routes = {};

    const addRoute = ({ authed, admin, driver, method, url, fn }) => {
        if (routes[method] === undefined) {
            routes[method] = {};
        }
        if (routes[method][url] === undefined) {
            routes[method][url] = {
                fn: fn,
                authed: authed,
                admin: admin,
                driver: driver
            };
        }
    };

    const route = (request, response) => {
        let { method: reqMethod, url: reqUrl, headers: reqHeaders } = request;
		
		var parseUrl = parser.parse(reqUrl,true);
		var parseData = parseUrl.query;
		reqUrl = parseUrl.pathname;

        if (
            routes[reqMethod] !== undefined &&
            routes[reqMethod][reqUrl] !== undefined
        ) {
            if (routes[reqMethod][reqUrl]["authed"] === true) {
                if (routes[reqMethod][reqUrl]["admin"] === true) {
                    routeAdmin(
                        request,
                        response,
                        reqHeaders,
                        routes[reqMethod][reqUrl]["fn"]
                    );
                } else if (routes[reqMethod][reqUrl]["driver"] === true) {
                    routeDriver(
                        request,
                        response,
                        reqHeaders,
                        routes[reqMethod][reqUrl]["fn"]
                    );
                } else {
                    routeAuthed(
                        request,
                        response,
                        reqHeaders,
                        routes[reqMethod][reqUrl]["fn"],
						parseData
                    );
                }
            } else {
                routeNotAuthed(
                    request,
                    response,
                    routes[reqMethod][reqUrl]["fn"]
                );
            }
        } else {
            writeResponse(
                {
                    header: {
                        status: 404
                    },
                    body: JSON.stringify({
                        status: "error",
                        message: "Route/item not found."
                    })
                },
                response
            );
        }
    };

    const use = callback => {
        callback().forEach(item => {
            if (routes[item.method] === undefined) {
                routes[item.method] = {};
            }
            if (routes[item.method][item.url] === undefined) {
                routes[item.method][item.url] = {
                    fn: item.fn,
                    authed: item.authed,
                    admin: item.admin,
                    driver: item.driver
                };
            }
        });
    };

    const writeResponse = function({ header, body }, response) {
        if (!response.finished) {
            response.writeHead(header.status, header.reason, header.headers);
            response.write(body);
            response.end();
        }
    };

    const routeNotAuthed = (request, response, doThis) => {
        return doThis(request, response);
    };

    const routeAuthed = (request, response, headers, doThis, parseData) => {
        token = cookie.getCookie(headers, "token");

        database
            .doQuery(`SELECT * FROM tokens WHERE token=?`, [token])
            .then(results => {
                if (results.length === 1) {
                    return database
                        .doQuery(`SELECT * FROM utilizatori WHERE id=?`, [
                            results[0]["id_utilizator"]
                        ])
                        .then(results => {
                            if (results.length) {
                                doThis(request, response, parseData);
                            } else {
                                console.log(
                                    `User ${
                                        results[0]["id"]
                                    } with token "${token}" is not client.`
                                );
                                writeResponse(
                                    {
                                        header: {
                                            status: 401
                                        },
                                        body: JSON.stringify({
                                            status: "error",
                                            message:
                                                "Forbidden. User not privileged."
                                        })
                                    },
                                    response
                                );
                            }
                        });
                } else {
                    console.log(`Forbidden: token "${token}" not found.`);
                    writeResponse(
                        {
                            header: {
                                status: 401
                            },
                            body: JSON.stringify({
                                status: "error",
                                message:
                                    "Forbidden. No token provided/token is invalid."
                            })
                        },
                        response
                    );
                }
            })
            .catch(error => {
                console.log(error);
                writeResponse(
                    {
                        header: {
                            status: 500
                        },
                        body: JSON.stringify({
                            status: "server error",
                            message: "Internal server error"
                        })
                    },
                    response
                );
            });
    };

    const routeAdmin = (request, response, headers, doThis) => {
        token = cookie.getCookie(headers, "token");

        database
            .doQuery(`SELECT * FROM tokens WHERE token=?`, [token])
            .then(results => {
                if (results.length === 1) {
                    return database
                        .doQuery(`SELECT * FROM utilizatori WHERE id=?`, [
                            results[0]["id_utilizator"]
                        ])
                        .then(results => {
                            if (results[0]["nivel_acces"] === 1) {
                                doThis(request, response);
                            } else {
                                console.log(
                                    `User ${
                                        results[0]["id"]
                                    } with token "${token}" is not admin.`
                                );
                                writeResponse(
                                    {
                                        header: {
                                            status: 401
                                        },
                                        body: JSON.stringify({
                                            status: "error",
                                            message:
                                                "Forbidden. User not privileged."
                                        })
                                    },
                                    response
                                );
                            }
                        });
                } else {
                    console.log(`Forbidden: token "${token}" not found.`);
                    writeResponse(
                        {
                            header: {
                                status: 401
                            },
                            body: JSON.stringify({
                                status: "error",
                                message:
                                    "Forbidden. No token provided/token is invalid."
                            })
                        },
                        response
                    );
                }
            })
            .catch(error => {
                console.log(error);
                writeResponse(
                    {
                        header: {
                            status: 500
                        },
                        body: JSON.stringify({
                            status: "server error",
                            message: "Internal server error"
                        })
                    },
                    response
                );
            });
    };

    const routeDriver = (request, response, headers, doThis) => {
        token = cookie.getCookie(headers, "token");

        database
            .doQuery(`SELECT * FROM tokens WHERE token=?`, [token])
            .then(results => {
                if (results.length === 1) {
                    return database
                        .doQuery(`SELECT * FROM utilizatori WHERE id=?`, [
                            results[0]["id_utilizator"]
                        ])
                        .then(results => {
                            if (results[0]["nivel_acces"] === 2) {
                                doThis(request, response);
                            } else {
                                console.log(
                                    `User ${
                                        results[0]["id"]
                                    } with token "${token}" is not driver.`
                                );
                                writeResponse(
                                    {
                                        header: {
                                            status: 401
                                        },
                                        body: JSON.stringify({
                                            status: "error",
                                            message:
                                                "Forbidden. User not privileged."
                                        })
                                    },
                                    response
                                );
                            }
                        });
                } else {
                    console.log(`Forbidden: token "${token}" not found.`);
                    writeResponse(
                        {
                            header: {
                                status: 401
                            },
                            body: JSON.stringify({
                                status: "error",
                                message:
                                    "Forbidden. No token provided/token is invalid."
                            })
                        },
                        response
                    );
                }
            })
            .catch(error => {
                writeResponse(
                    {
                        header: {
                            status: 500
                        },
                        body: JSON.stringify({
                            status: "server error",
                            message: "Internal server error"
                        })
                    },
                    response
                );
            });
    };

    return {
        addRoute,
        route,
        use,
        writeResponse
    };
})();
