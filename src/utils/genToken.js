module.exports = (() => {
    const characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    const genToken = (size) => {
        let token = "";

        for (let i = 0; i < size; ++i) {
            token += characters[Math.floor(Math.random() * (characters.length))];
        }

        return token;
    }

    return genToken;
})();