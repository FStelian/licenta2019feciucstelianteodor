module.exports = (() => {
    const getBody = request => {
        return new Promise((resolve, reject) => {
            let body = "";
            request.on("data", data => body += data)
            request.on("end", () => resolve(body));
            request.on("error", () => refuse());
        })
    }

    const parseBody = data => {
        return new Promise((resolve, reject) => {
            try {
                resolve(JSON.parse(data))
            }
            catch (error) {
                reject({
                    header: {
                        status: 500
                    },
                    body: JSON.stringify({
                        status: "server error",
                        message: "Internal server error"
                    })
                });
            }
        })
    }

    return {
        getBody,
        parseBody
    };
})();