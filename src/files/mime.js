module.exports = {
    getMime: function (url) {
        var mime;
        var extension = url.match("[.][^.]*");

        switch (extension[0]) {
            case '.js':
                mime = 'application/javascript';
                break;
            case '.css':
                mime = 'text/css';
                break;
            case '.html':
                mime = 'text/html';
                break;
			case '.ejs':
				mime = 'text/html';
				break;
            case '.png':
                mime = 'image/png';
                break;
            case '.jpg':
            case '.jpeg':
                mime = 'image/jpeg';
                break;
            case '.svg':
                mime = 'image/svg+xml';
                break;
            default:
                mime = 'text/plain';
        }

        return mime;
    }
}
