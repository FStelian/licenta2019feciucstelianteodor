module.exports = (() => {
    const http = require("http");
    const db = require("../db/database");
    const { getBody, parseBody } = require("../helpers/helpers");
    const { writeResponse } = require("../router");

    const defaultPath = "/route/v1/driving/";
    let options = {
        host: "0.0.0.0",
        port: 5000,
        path: "/route/v1/driving/"
    };

    const getMapRoute = (request, response) => {
        getBody(request)
            .then(parseBody)
            .then(getFromDatabase)
            .then(findClosestTwoVehicles)
            .then(getClosestPointRouteStations)
            .then(createClosestPointRouteURL)
            .then(requestURL)
            .then(parseBody)
            .then(getSecondClosestPointRouteStations)
            .then(createSecondClosestPointRouteURL)
            .then(requestURL)
            .then(parseBody)
            .then(getQueriedRouteStations)
            .then(createQueriedRouteStationsURL)
            .then(requestURL)
            .then(parseBody)
            .then(sendResponse.bind(null, response))
            .catch(sendError.bind(null, response));
    };

    const getFromDatabase = body => {
        return db.doQuery("SELECT * FROM puncte WHERE id_ruta=? and coordonate_lat=? and coordonate_long=? order by ordine asc", [body.id, body.firstCoordinates[1], body.firstCoordinates[0]])
        .then(stations => {
            if (stations[0]) {
                return db.doQuery("SELECT * FROM puncte WHERE id_ruta=? and coordonate_lat=? and coordonate_long=? order by ordine asc", [body.id, body.secondCoordinates[1], body.secondCoordinates[0]])
                .then(destinationStaiton => {
                    return db.doQuery("SELECT * FROM puncte p JOIN vehicule_transport v on p.id = v.statie_noua WHERE v.id_ruta = ?", [body.id])
                    .then(statiiCurenteVehicule => {
                        var directieInainte = 1;
                        if (stations[0].ordine > destinationStaiton[0].ordine) {
                            directieInainte = 0;
                        }
                        return {
                            queriedPoint: stations[0],
                            destionationPoint: destinationStaiton[0],
                            statiiCurenteVehicule: statiiCurenteVehicule,
                            directieInainte: directieInainte,
                            id: body.id
                        }
                    });
                });
            }
            else {
                throw {
                    status: 400,
                    message: "Staţie invalidă"
                }
            }
        })
        .catch(error => {
            if (typeof error === "object") {
                throw error;
            }
            else {
                throw {
                    status: 500,
                    message: "Internal server error"
                }
            }
        })
    };

    const getClosestPointRouteStations = async data => {
        var query = "SELECT * FROM puncte WHERE id_ruta=? and (ordine >= ? and ordine <= ?) order by ordine asc";
        if (data.requestedData.closestPointDirection == 0) {
            if (data.requestedData.closestPoint.ordine < data.requestedData.queriedPoint.ordine) {
                query = "SELECT * FROM puncte WHERE id_ruta=? and (ordine >= ? and ordine <= ?) order by ordine desc";
            } else {
                query = "SELECT * FROM puncte WHERE id_ruta=? and (ordine <= ? and ordine >= ?) order by ordine desc";
            }
        } else {
            if (data.requestedData.closestPoint.ordine < data.requestedData.queriedPoint.ordine) {
                query = "SELECT * FROM puncte WHERE id_ruta=? and (ordine >= ? and ordine <= ?) order by ordine desc";
            } else {
                query = "SELECT * FROM puncte WHERE id_ruta=? and (ordine <= ? and ordine >= ?) order by ordine desc";
            }
        }
        return await db.doQuery(query, [data.requestedData.id, data.requestedData.closestPoint.ordine, data.requestedData.queriedPoint.ordine])
        .then(stations => {
            data.requestedData.closestPointRouteStations = stations;
            return data;
        })
        .catch(error => {
            if (typeof error === "object") {
                throw error;
            }
            else {
                console.log(error);
                throw {
                    status: 500,
                    message: "Internal server error"
                }
            }
        });
    };

    const getSecondClosestPointRouteStations = async data => {
        var query = "SELECT * FROM puncte WHERE id_ruta=? and (ordine >= ? and ordine <= ?) order by ordine asc";
        if (data.requestedData.secondClosestPointDirection == 0) {
            if (data.requestedData.secondClosestPoint.ordine < data.requestedData.queriedPoint.ordine) {
                query = "SELECT * FROM puncte WHERE id_ruta=? and (ordine >= ? and ordine <= ?) order by ordine desc";
            } else {
                query = "SELECT * FROM puncte WHERE id_ruta=? and (ordine <= ? and ordine >= ?) order by ordine desc";
            }
        } else {
            if (data.requestedData.secondClosestPoint.ordine < data.requestedData.queriedPoint.ordine) {
                query = "SELECT * FROM puncte WHERE id_ruta=? and (ordine >= ? and ordine <= ?) order by ordine desc";
            } else {
                query = "SELECT * FROM puncte WHERE id_ruta=? and (ordine <= ? and ordine >= ?) order by ordine desc";
            }
        }
        return await db.doQuery(query, [data.requestedData.id, data.requestedData.secondClosestPoint.ordine, data.requestedData.queriedPoint.ordine])
            .then(stations => {
                data.requestedData.secondClosestPointRouteStations = stations;
                return data;
            })
            .catch(error => {
                if (typeof error === "object") {
                    throw error;
                }
                else {
                    console.log(error);
                    throw {
                        status: 500,
                        message: "Internal server error"
                    }
                }
            })
    };

    const getQueriedRouteStations = async data => {
        // console.log(body);
        var query = "SELECT * FROM puncte WHERE id_ruta=? and (ordine >= ? and ordine <= ?) order by ordine asc";
        if (data.requestedData.directieInainte == 0) {
            query = "SELECT * FROM puncte WHERE id_ruta=? and (ordine <= ? and ordine >= ?) order by ordine desc";
        } 
        return await db.doQuery(query, [data.requestedData.id, data.requestedData.queriedPoint.ordine, data.requestedData.destionationPoint.ordine])
            .then(stations => {
                data.requestedData.queriedRouteStations = stations;
                return data;
            })
            .catch(error => {
                if (typeof error === "object") {
                    throw error;
                }
                else {
                    console.log(error);
                    throw {
                        status: 500,
                        message: "Internal server error"
                    }
                }
            })
    };

    const createClosestPointRouteURL = data => {
        let path = defaultPath;

        data.requestedData.closestPointRouteStations.forEach(station => {
            path += `${station.coordonate_long},${station.coordonate_lat};`;
        });

        if (data.requestedData.closestPointRouteStations.length == 1) {
            var station = data.requestedData.closestPointRouteStations[0];
            path += `${station.coordonate_long},${station.coordonate_lat};`;
        }

        path = path.substr(0, path.length - 1);
        options.path = path;

        // console.log(data.requestedData);

        return data;
    };

    const createSecondClosestPointRouteURL = data => {
        let path = defaultPath;

        data.requestedData.secondClosestPointRouteStations.forEach(station => {
            path += `${station.coordonate_long},${station.coordonate_lat};`;
        });

        if (data.requestedData.secondClosestPointRouteStations.length == 1) {
            var station = data.requestedData.secondClosestPointRouteStations[0];
            path += `${station.coordonate_long},${station.coordonate_lat};`;
        }

        path = path.substr(0, path.length - 1);
        options.path = path;
        return data;
    };

    const createQueriedRouteStationsURL = data => {
        let path = defaultPath;

        data.requestedData.queriedRouteStations.forEach(station => {
            path += `${station.coordonate_long},${station.coordonate_lat};`;
        });

        if (data.requestedData.queriedRouteStations.length == 1) {
            var station = data.requestedData.queriedRouteStations[0];
            path += `${station.coordonate_long},${station.coordonate_lat};`;
        }

        path = path.substr(0, path.length - 1);
        options.path = path;
        return data
    };

    const findClosestTwoVehicles = data => {
        var minClosest = Infinity;
        var minSecondClosest = Infinity;
        var foundClosestPoint = false;
        var foundSecondClosestPoint = false;
        var closestPoint, secondClosestPoint;
        for (const statieVehicul of data.statiiCurenteVehicule) {
            var stationsDistance = data.queriedPoint.ordine - statieVehicul.ordine;
            if (statieVehicul.directie_inainte == data.directieInainte) {
                if (stationsDistance < minClosest && stationsDistance > 0) {
                    closestPoint = statieVehicul;
                    foundClosestPoint = true;
                    minClosest = stationsDistance;
                }
                if (stationsDistance < minSecondClosest && stationsDistance > 0 && closestPoint.id != statieVehicul.id) {
                    secondClosestPoint = statieVehicul;
                    foundSecondClosestPoint = true;
                    minSecondClosest = stationsDistance;
                }
            }
        }
        minFirst = Infinity;
        minSecond = Infinity;
        for (const statieVehicul of data.statiiCurenteVehicule) {
            var stationsDistance = Math.abs(data.queriedPoint.ordine - statieVehicul.ordine);
            if (stationsDistance < minClosest && foundClosestPoint == false) {
                secondClosestPoint = closestPoint;
                minSecondClosest = minClosest;
                closestPoint = statieVehicul;
                minClosest = stationsDistance;
            }
            if (stationsDistance < minSecondClosest && foundSecondClosestPoint == false && closestPoint.id != statieVehicul.id) {
                secondClosestPoint = statieVehicul;
                minSecondClosest = stationsDistance;
            }
        }

        data.requestedData = JSON.parse(JSON.stringify(data));
        data.requestedData.closestPoint = closestPoint;

        if (foundClosestPoint == true) {
            data.requestedData.closestPointDirection = data.requestedData.directieInainte; 
        } else {
            data.requestedData.closestPointDirection = data.requestedData.directieInainte == 1 ? 0 : 1;
        }

        if (secondClosestPoint && secondClosestPoint.id) {
            data.requestedData.secondClosestPoint = secondClosestPoint;
        } else {
            data.requestedData.secondClosestPoint = closestPoint;
            foundClosestPoint = true;
        }

        if (foundSecondClosestPoint == true) {
            data.requestedData.secondClosestPointDirection = data.requestedData.directieInainte; 
        } else {
            data.requestedData.secondClosestPointDirection = data.requestedData.directieInainte == 1 ? 0 : 1;
        }

        // console.log(data)

        return data;
    };

    const requestURL = requestedData => {
        return new Promise((resolve, reject) => {
            let data = "";

            const req = http.request(options, res => {
                res.on("data", chunk => {
                    data += chunk;
                });
                res.on("end", () => {
                    var body = JSON.parse(data)
                    let parsedData = JSON.parse(data);
                    if (requestedData.requestedData){
                        parsedData.requestedData = requestedData.requestedData;
                        if (parsedData.requestedData.estimates == null) {
                            parsedData.requestedData.estimates = [];
                        }
                        parsedData.requestedData.estimates.push({
                            stationName: body.waypoints[0].name,
                            duration: body.routes[0].duration,
                            distance: body.routes[0].distance,
                            geometry: body.routes[0].geometry,
                        });
                    }
                    else {
                        parsedData.requestedData = requestedData
                    }
                    resolve(JSON.stringify(parsedData))
                });
                res.on("error", error => reject(error));
            });

            req.on("error", error => reject(error));
            req.end();
        });
    };

    const sendResponse = (response, body) => {
        if (body.requestedData.estimates[0].duration > body.requestedData.estimates[1].duration) {
            var aux = body.requestedData.estimates[0];
            body.requestedData.estimates[0] = body.requestedData.estimates[1];
            body.requestedData.estimates[1] = aux;
        }
        writeResponse(
            {
                header: {
                    status: 200
                },
                body: JSON.stringify(body.requestedData.estimates)
            },
            response
        );

        return body;
    };

    const sendError = (response, error) => {
        console.log(error);
        writeResponse(
            {
                header: {
                    status: error.status
                },
                body: error.message
            },
            response
        );
    };

    return getMapRoute;
})();
