module.exports = (() => {
    const db = require("../db/database");
    const {
        getBody,
        parseBody
    } = require("../helpers/helpers");
    const {
        writeResponse
    } = require("../router");

    const getMapRouteById = (request, response) => {
        getBody(request)
            .then(parseBody)
            .then(body => {
                return validateBody(body)
                    .then(getFromDatabase.bind(null, body))
                    .then(sendResponse.bind(null, response, undefined))
            })
            .catch(sendError.bind(null, response))
    }

    const validateBody = body => {
        return new Promise((resolve, reject) => {
            if (typeof body.id === "string" ||
                !(/[A-Za-z0-9 ]+/.test(body.id))) {
                resolve()
            } else {
                reject({
                    status: 500,
                    message: "Id nu este string"
                })
            }
        })
    }

    const getFromDatabase = body => {
        return db.doQuery("SELECT * FROM rute WHERE id=?", [body.id])
            .then(route => {
                if (!route[0]) {
                    throw {
                        status: 404,
                        message: "Rută negăsită"
                    }
                }
                return db.doQuery("SELECT * FROM puncte WHERE id_ruta=? and tip='station' order by ordine asc", [route[0].id])
                    .then(points => {
                        let pointsArray = [];
                        points.forEach(item => pointsArray.push({
                            type: item.tip,
                            coordinates: [item.coordonate_long, item.coordonate_lat],
                            order: item.ordine
                        }));
                        return db.doQuery("SELECT v.id as 'id', v.directie_inainte as 'directie_inainte', p.coordonate_long as 'coordonate_long', p.coordonate_lat as 'coordonate_lat' FROM vehicule_transport v join puncte p on v.statie_noua = p.id WHERE v.id_ruta=? order by id asc", [route[0].id])
                        .then(vehicule => {
                            let vehiculeIds = [];
                            for (var i = 0; i < vehicule.length; i++) {
                                vehiculeIds.push(vehicule[i].id);
                                vehicule[i].coordinates = [vehicule[i].coordonate_long, vehicule[i].coordonate_lat]
                                
                            }
                            return db.doQuery("SELECT * FROM coduri_qr where id_vehicul=?", [vehicule[0].id])
                            .then(qr => {
                                return {
                                    points: pointsArray,
                                    qr: qr[0].qr,
                                    vehiculeIds: vehiculeIds,
                                    vehicule: vehicule
                                }
                            })
                        })
                    })
            })
            .catch(error => {
                if (typeof error === "object") {
                    throw error;
                } else {
                    throw {
                        status: 500,
                        message: "Internal server error",
                        error: error
                    }
                }
            })
    }

    const sendResponse = (response, headers, body) => {
        writeResponse({
                header: {
                    status: 200,
                    reason: "OK",
                    headers: headers
                },
                body: JSON.stringify(body)
            },
            response
        );
    };

    const sendError = (response, error) => {
        writeResponse({
                header: {
                    status: error.status
                },
                body: JSON.stringify(error)
            },
            response
        );
    };

    return getMapRouteById;
})();