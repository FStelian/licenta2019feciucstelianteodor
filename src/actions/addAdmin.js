module.exports = (() => {
    const db = require("../db/database");
    const { writeResponse } = require("../router");

    const getBody = request => {
        return new Promise((resolve, reject) => {
            let body = "";
            request.on("data", data => (body += data));
            request.on("end", () => resolve(body));
            request.on("error", error =>
                reject({
                    status: 500,
                    message: "Internal server error",
                    error: error
                })
            );
        });
    };

    const addAdmin = (request, response) => {
        getBody(request)
            .then(body => {
                body = JSON.parse(body);
                return db
                    .doQuery(
                        `INSERT INTO utilizatori( username, password, email, nume, prenume, varsta, nivel_acces, numar_bilete, id_bilet, nr_telefon) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`,
                        [body.username, body.password, body.email, body.nume, body.prenume, body.varsta, body.nivel_acces, 0, 1, body.nr_telefon]
                    ).then(() => {
                        writeResponse(
                            {
                                header: {
                                    status: 200,
                                    reason: "OK"
                                },
                                body: JSON.stringify({
                                    status: 200,
                                    message: "Utilizator Adaugat"
                                })
                            },
                            response
                        )
                    }).catch(error => {
                        throw {
                            status: 400,
                            message: "Bad Request",
                            error: error
                        };
                    });
            }).catch(error => {
            throw {
                status: 400,
                message: "Bad Request",
                error: error
            };
        });
    };

    return addAdmin;
})();
