module.exports = (() => {
    const db = require("../db/database");
    const genToken = require("../utils/genToken");
    const {
        getBody,
        parseBody
    } = require("../helpers/helpers");
    const {
        writeResponse
    } = require("../router");

    const editMapRoute = (request, response) => {
        getBody(request)
            .then(parseBody)
            .then(body => {
                return validateBody(body)
                    .then(insertIntoDatabase.bind(null, body, response))
            })
            .catch(sendError.bind(null, response))
    }

    const validateBody = body => {
        return new Promise((resolve, reject) => {
            body.points.forEach(item => {
                if ((item.type !== "station" &&
                        item.type !== "point") ||
                    !Array.isArray(item.coordinates) ||
                    typeof item.coordinates[0] !== "number" ||
                    typeof item.coordinates[1] !== "number") {
                    reject({
                        status: 400,
                        message: "Bad request"
                    })
                }
            })
            if (typeof body.number !== "string" ||
                !(/[A-Za-z0-9 ]+/.test(body.number))) {
                reject({
                    status: 400,
                    message: "Bad request"
                })
            } else {
                resolve();
            }

        })
    }

    const insertIntoDatabase = (body, response) => {
        return db.doQuery("SELECT * FROM vehicule_transport WHERE id_ruta=?", [body.number])
            .then(async vehicles => {
                if (vehicles[0]) {
                    await db.doQuery("DELETE FROM puncte WHERE id_ruta=?", body.number)
                        .catch(error => {
                            throw {
                                status: 500,
                                message: "Internal server error"
                            }
                        });
                    for (const point of body.points) {
                        var firstStation;
                        var insertResult = await db.doQuery(
                            "INSERT INTO puncte (id_ruta, tip, coordonate_long, coordonate_lat, ordine) VALUES (?, ?, ?, ?, ?)", 
                            [body.number, point.type, point.coordinates[0], point.coordinates[1], point.order]
                        );
                        if (point.order == 1) {
                            firstStation = insertResult.insertId;
                        }
                    }
                    var qr;
                    var vehiculeIds = [];
                    for (var i = 0; i < vehicles.length; i++) {
                        const token = genToken(100);
                        qr = token;
                        await db.doQuery("INSERT INTO vehicule_transport (id_ruta, statie_noua) VALUES (?, ?)", [body.number, firstStation]).then(resultInsertVehicul => {
                            vehiculeIds.push(resultInsertVehicul.insertId);
                            return db.doQuery("INSERT INTO coduri_qr (id_vehicul, qr) VALUES (?, ?)", [resultInsertVehicul.insertId, token]);
                        });
                    }
                    sendResponse(response, undefined, {
                        status: 200,
                        message: "Rută modificată",
                        qr: qr,
                        vehiculeIds: vehiculeIds
                    });
                } else {
                    throw {
                        status: 400,
                        message: "Bad request"
                    }
                }
            })
            .catch(error => {
                if (typeof error === "object") {
                    throw error;
                } else {
                    throw {
                        status: 500,
                        message: "Internal server error",
                    }
                }
            })
    }

    const sendResponse = (response, headers, body) => {
        writeResponse({
                header: {
                    status: 200,
                    reason: "OK",
                    headers: headers
                },
                body: JSON.stringify(body)
            },
            response
        );
    };

    const sendError = (response, error) => {
        console.log(error);
        writeResponse({
                header: {
                    status: error.status
                },
                body: JSON.stringify(error)
            },
            response
        );
    };

    return editMapRoute;
})();