module.exports = (() => {
    const db = require("../db/database");
    const {
        writeResponse
    } = require("../router");

    const getAllMapRoutes = (request, response) => {
        getFromDatabase()
            .then(sendResponse.bind(null, response, undefined))
            .catch(sendError.bind(null, response))
    }

    const getFromDatabase = () => {
        return new Promise((resolve, reject) => {
            db.doQuery("SELECT * FROM rute")
                .then(routes => {
                    let routesArray = [];
                    routes.forEach(item => routesArray.push(item.nume))
                    resolve({
                        routes: routesArray
                    });
                })
                .catch(error => {
                    reject({
                        status: 500,
                        message: "Internal server error"
                    })
                })
        })
    }

    const sendResponse = (response, headers, body) => {
        writeResponse({
                header: {
                    status: 200,
                    reason: "OK",
                    headers: headers
                },
                body: JSON.stringify(body)
            },
            response
        );
    };

    const sendError = (response, error) => {
        writeResponse({
                header: {
                    status: error.status
                },
                body: JSON.stringify(error)
            },
            response
        );
    };

    return getAllMapRoutes;
})();