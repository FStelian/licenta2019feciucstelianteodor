module.exports = (() => {
    const db = require("../db/database");
    const {
        writeResponse
    } = require("../router");
    const {
        getBody,
        parseBody
    } = require("../helpers/helpers");

    const register = (request, response) => {
        getBody(request)
            .then(parseBody)
            .then(body => {
                return validateBody(body)
                    .then(() => {
                        return db.doQuery("SELECT max(id) AS \"id\" from utilizatori")
                            .then(maxId => {
                                return db.doQuery(`SELECT * FROM utilizatori WHERE username=? or email=?`, [
                                        body.username,
                                        body.email
                                    ])
                                    .then(result => {
                                        if (result.length === 1) {
                                            throw {
                                                status: 400,
                                                message: "Username/e-mail există deja"
                                            }
                                        } else {
                                            return db
                                                .doQuery(
                                                    `INSERT INTO utilizatori (id, username, password, email, nume, prenume, varsta, nr_telefon, numar_bilete, id_bilet) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`, [
                                                        maxId[0].id + 1,
                                                        body.username,
                                                        body.password,
                                                        body.email,
                                                        body.lastname,
                                                        body.firstname,
                                                        body.age,
                                                        body.phone,
                                                        0,
                                                        1
                                                    ])
                                                .then(() => {
                                                    return;
                                                })
                                                .catch(error => {
                                                    throw {
                                                        status: 500,
                                                        message: "Internal server error"
                                                    }
                                                });
                                        }
                                    })
                                    .catch(error => {
                                        if (typeof error === "object") {
                                            throw error;
                                        } else {
                                            throw {
                                                status: 500,
                                                message: "Internal server error"
                                            }
                                        }
                                    })
                            })
                            .catch(error => {
                                if (typeof error === "object") {
                                    throw error;
                                } else {
                                    throw {
                                        status: 500,
                                        message: "Internal server error"
                                    }
                                }
                            })
                    })
                    .then(() => {
                        writeResponse({
                                header: {
                                    status: 301,
                                },
                                body: JSON.stringify({
                                    status: 200,
                                    response: "Utilizator creat",
                                    goto: "index.page"
                                })
                            },
                            response
                        );
                    })
                    .catch(error => {
                        writeResponse({
                            header: {
                                status: error.status
                            },
                            body: JSON.stringify(error)
                        }, response)
                    })
            })
    };

    const validateBody = body => {
        return new Promise((resolve, reject) => {
            let errors = "";

            if (body.username === undefined || !/^[A-Za-z0-9\.]+$/.test(body.username)) {
                errors += "Username invalid\n";
            }
            if (body.password === undefined || !/^[A-Za-z0-9\.!@]+$/.test(body.password)) {
                errors += "Parolă invalidă\n";
            }
            if (body.confirmpassword === undefined || !/^[A-Za-z0-9\.!@]+$/.test(body.confirmpassword)) {
                errors += "Confirmare parolă invalidă\n";
            }
            if (body.firstname === undefined || !/^[A-Za-z0-9-]+$/.test(body.firstname)) {
                errors += "Nume invalid\n";
            }
            if (body.lastname === undefined || !/^[A-Za-z0-9-]+$/.test(body.lastname)) {
                errors += "Prenume invalid\n";
            }
            if (body.age === undefined || !/^[1-9][0-9]+$|^[1-9]$/.test(body.age)) {
                errors += "Vârstă invalidă\n";
            }
            if (body.email === undefined || !/^[A-Za-z0-9][A-Za-z0-9-_\.]*(@)[a-z0-9]+(\.)[a-z]+$/.test(body.email)) {
                errors += "E-mail invalid\n";
            }
            if (body.phone === undefined || !/(02|07)[0-9]{8}/.test(body.phone)) {
                errors += "Număr de telefon invalid\n";
            }
            if (body.password !== body.confirmpassword) {
                errors += "Parolele nu coincid\n";
            }

            if (errors !== "") {
                reject({
                    status: 400,
                    message: errors
                })
            } else {
                resolve();
            }
        })
    }
    return register;
})();