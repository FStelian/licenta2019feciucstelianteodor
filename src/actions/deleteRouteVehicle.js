module.exports = (() => {
    const db = require("../db/database");
    const { writeResponse } = require("../router");
    const { getBody, parseBody } = require("../helpers/helpers");
	
    const insertIntoDatabase = body => {
        return db.doQuery('DELETE FROM vehicule_transport WHERE id = ?', [body.id]).then(result => {
            if (result.affectedRows == 1) {
                return {
                    success: true,
                    message: "Vehicul șters"
                }
            } else {
                return {
                    success: false,
                    message: "Vehiculul s-a putut șterge"
                }
            }
        });
    };

    const deleteRoutehicle = (request, response) => {
        getBody(request)
            .then(parseBody)
            .then(body => {
                return validateBody(body)
                    .then(insertIntoDatabase.bind(null, body))
                    .then(sendResponse.bind(null, response, undefined))
            })
            .catch(sendError.bind(null, response))
    }

    const validateBody = body => {
        return new Promise((resolve, reject) => {
            if (typeof body.id === "string" ||
                !(/[A-Za-z0-9 ]+/.test(body.id))) {
                resolve()
            } else {
                reject({
                    status: 500,
                    message: "Id nu este string"
                })
            }
        })
    }

    const sendResponse = (response, headers, body) => {
        writeResponse({
                header: {
                    status: 200,
                    reason: "OK",
                    headers: headers
                },
                body: JSON.stringify(body)
            },
            response
        );
    };

    const sendError = (response, error) => {
        writeResponse({
                header: {
                    status: error.status
                },
                body: JSON.stringify(error)
            },
            response
        );
    };

	return deleteRoutehicle;
})();
