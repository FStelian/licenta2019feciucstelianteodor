module.exports = (() => {
    const db = require("../db/database");
    const { setCookie, getCookie } = require("../cookie");
    const { writeResponse } = require("../router");

    const logout = (request, response) => {
        let { headers } = request;
        const token = getCookie(headers, "token");
        db.doQuery(`DELETE FROM tokens WHERE token=?`, [token])
            .then(() => {
                let newHeaders = setCookie("token", "");
                newHeaders["Location"] = "/index.page";
                writeResponse(
                    {
                        header: {
                            status: 301,
                            reason: "Logged out",
                            headers: newHeaders
                        },
                        body: JSON.stringify({
                            status: 200,
                            message: "Logged out"
                        })
                    },
                    response
                );
            })
            .catch(error => {
                writeResponse(
                    {
                        header: {
                            status: 500
                        },
                        body: JSON.stringify({
                            status: 500,
                            error: "Internal server error"
                        })
                    },
                    response
                );
            });
    };

    return logout;
})();
