function formTransition(formid1, formid2) {
    var fadeOutForm = document.getElementById(formid1);
    var fadeInForm = document.getElementById(formid2);
    fadeOutForm.classList.add("fadeOut");
    setTimeout(function() {
        fadeOutForm.classList.add("hide");
        fadeOutForm.classList.remove("fadeOut");
        fadeInForm.classList.remove("hide");
        fadeInForm.classList.add("fadeIn");
        fadeInForm.classList.remove("fadeIn");
    }, 500);
}
