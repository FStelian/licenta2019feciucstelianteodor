processForm = e => {
    e.preventDefault();
    const form = e.target;
    let data = {};

    const inputs = form.getElementsByTagName("input");
    for (let input of inputs) {
        if (input.type !== "submit") {
            data[input.name] = input.value;
        }
    }
    const selects = form.getElementsByTagName("select");
    for (let select of selects) {
        data[select.name] = select.options[select.selectedIndex].value;
    }

    fetch(form.action, {
        method: form.id,
        credentials: "same-origin",
        body: JSON.stringify(data)
    })
        .then(response => response.json())
        .then(response => {
            if (response["goto"] !== undefined) {
                window.location = response["goto"];
            } else {
                if (response.status < 400) {
                    Swal.fire({
                        type: 'success',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 1200
                    }); 
                } else {
                    Swal.fire({
                        type: 'error',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 1200
                    }); 
                }
            }
        });

    return false;
};

const formSubmitListener = item => {
    item.addEventListener(
        "submit",
        function(event) {
            processForm(event);
        },
        false
    );
};

const forms = document.getElementsByTagName("form");
for (let item of forms) {
    formSubmitListener(item);
}
