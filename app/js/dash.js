(() => {
    const routeGetByIdURL = "/routes/byId";
    const routeGetEstimate = "/estimate";
    const route = {
        name: "",
        polylines: [],
        features: [],
        points: []
    };
    const styles = {
        route: new ol.style.Style({
            stroke: new ol.style.Stroke({
                width: 5,
                color: [40, 40, 40, 1]
            })
        }),
        station: new ol.style.Style({
            image: new ol.style.Icon({
                scale: 0.3,
                src: 'res/station.png'
            })
        }),
        busgoing: new ol.style.Style({
            image: new ol.style.Icon({
                scale: 0.3,
                src: 'res/busgoing.png'
            })
        }),
        buscoming: new ol.style.Style({
            image: new ol.style.Icon({
                scale: 0.3,
                src: 'res/buscoming.png'
            })
        })
    };

    var firstCoordinates = [];
    var secondCoordinates = [];

    const createMapOverlay = () => {
        return new Promise((resolve, reject) => {
            const mapDialog = document.getElementsByClassName("mapDialog")[0];
            const overlay = new ol.Overlay({
                element: mapDialog,
                autoPan: true,
                autoPanAnimation: {
                    duration: 250
                }
            });
            resolve(overlay);
        });
    };

    const createMap = overlay => {
        return new Promise((resolve, reject) => {
            const map = new ol.Map({
                layers: [
                    new ol.layer.Tile({
                        source: new ol.source.OSM()
                    })
                ],
                target: "largeMap",
                controls: ol.control.defaults({
                    attributionOptions: {
                        collapsible: false
                    }
                }),
                view: new ol.View({
                    center: ol.proj.fromLonLat([27.6014, 47.1555]),
                    zoom: 13
                }),
                overlays: [overlay]
            });

            resolve(map);
        });
    };

    const createVectorLayer = map => {
        return new Promise((resolve, reject) => {
            const vectorSource = new ol.source.Vector({});
            const vectorLayer = new ol.layer.Vector({
                source: vectorSource
            });
            map.addLayer(vectorLayer);

            resolve(vectorSource);
        });
    };

    const addClickHandlerForMap = (map, overlay, vectorSource) => {
        return new Promise((resolve, reject) => {
            map.on("click", event => {
                overlay.setPosition(undefined);
                map.forEachFeatureAtPixel(event.pixel, (feature, layer) => {
                    const index = route.features.findIndex(item => item === feature);
                    
                    if (firstCoordinates.length == 0) {
                        firstCoordinates = route.points[index];
                    } else {
                        secondCoordinates = route.points[index];
                        fetchResult(firstCoordinates, secondCoordinates, overlay, vectorSource);
                        firstCoordinates = [];
                        secondCoordinates = [];
                    }
                });
            });
            resolve();
        })
    };

    const fetchResult = (firstCoordinates, secondCoordinates, overlay, vectorSource) => {
        fetch(routeGetEstimate, {
                method: "POST",
                credentials: "same-origin",
                body: JSON.stringify({
                    firstCoordinates: [Number(firstCoordinates[0].toFixed(8)), Number(firstCoordinates[1].toFixed(8))],
                    secondCoordinates: [Number(secondCoordinates[0].toFixed(8)), Number(secondCoordinates[1].toFixed(8))],
                    id: document.getElementById("select_rute").getAttribute("data-id")
                })
            })
            .then(data => data.json())
            .then(body => {
                while (route.polylines.length > 0) {
                    vectorSource.removeFeature(route.polylines[0]);
                    route.polylines.shift();
                }
                overlay.setPosition(ol.proj.fromLonLat(firstCoordinates));
                const spans = overlay.element.getElementsByTagName("span");
                spans[1].innerHTML = '';
                spans[3].innerHTML = '';
                spans[4].innerHTML = '';
                spans[5].innerHTML = '';
                spans[6].innerHTML = '';
                spans[0].innerHTML = "Primul mijloc de transport este la "+ body[0].distance + " metri distanță și o să ajungă in  " + Math.floor(body[0].duration.toFixed(3) / 60) + " minute și " + Math.floor(body[0].duration.toFixed(3) % 60) + " secunde"
                spans[2].innerHTML = "Al doilea mijloc de transport este la " + body[1].distance + " metri distanță și o să ajungă in " + Math.floor(body[1].duration.toFixed(3) / 60) + " minute și " + Math.floor(body[1].duration.toFixed(3) % 60) + " secunde";
                spans[4].innerHTML = "O să vă ia " + Math.floor(body[2].duration.toFixed(3) / 60) + " minute și " + Math.floor(body[2].duration.toFixed(3) % 60) + " secunde să ajungeți la destinație";
                const routeGeometry = new ol.format.Polyline({
                    factor: 1e5
                }).readGeometry(body[2].geometry, {
                    dataProjection: "EPSG:4326",
                    featureProjection: "EPSG:3857"
                });
    
                const feature = new ol.Feature({
                    type: "route",
                    geometry: routeGeometry
                });
    
                feature.setStyle(styles.route);
                vectorSource.addFeature(feature);
                route.polylines.push(feature);
            })
    }

    const addHandlerForSelect = (vectorSource) => {
        return new Promise((resolve, reject) => {
            const select = document.getElementById("select_rute");
            select.addEventListener("change", editFormSelectHandler.bind(null, vectorSource));
            resolve();
        })
    }

    const editFormSelectHandler = (vectorSource, event) => {
        if (event.target.value !== "") {
            while (route.polylines.length > 0) {
                vectorSource.removeFeature(route.polylines[0]);
                route.polylines.shift();
            }
            const select = document.getElementById("select_rute");
            const optionData = select.options[select.selectedIndex].getAttribute("data-id");
            select.setAttribute("data-id", optionData);
            fetch(routeGetByIdURL, {
                    method: "POST",
                    credentials: "same-origin",
                    body: JSON.stringify({
                        id: document.getElementById("select_rute").getAttribute("data-id")
                    })
                })
                .then(data => data.json())
                .then(body => {
                    cleanUp(vectorSource);
                    for (var i = 0; i < body.vehicule.length; i++) {
                        for (var j = 0; j < body.points.length; j++) {
                            if (body.points[j].coordinates[0] == body.vehicule[i].coordinates[0] &&
                                body.points[j].coordinates[1] == body.vehicule[i].coordinates[1]) {
                                    body.points.splice(j, 1);
                                }
                        }
                    }
                    for (var i = 0; i < body.vehicule.length; i++) {
                        for (var j = i + 1; j < body.vehicule.length; j++) {
                            if (body.vehicule[j].coordinates[0] == body.vehicule[i].coordinates[0] &&
                                body.vehicule[j].coordinates[1] == body.vehicule[i].coordinates[1]) {
                                    body.vehicule.splice(j, 1);
                                }
                        }
                    }
                    addBodyPoints(vectorSource, body.points);
                    for (var i = 0; i < body.vehicule.length; i++) {
                        if (body.vehicule[i].directie_inainte == 1) {
                            body.vehicule[i].type = 'buscoming';
                        } else {
                            body.vehicule[i].type = 'busgoing';
                        }
                    }
                    addBodyPoints(vectorSource, body.vehicule);
                })
        }
    }

    const cleanUp = vectorSource => {
        while (route.features.length > 0) {
            vectorSource.removeFeature(route.features[0]);
            route.features.shift();
        }

        route.points = [];
        route.features = [];
    }

    const addBodyPoints = (vectorSource, points) => {
        if (points.length > 0) {
            mapFlow(vectorSource, points[0].coordinates, points[0].type)
                .then(() => {
                    points.shift();
                    addBodyPoints(vectorSource, points);
                })
        }
    }

    const mapFlow = (vectorSource, point, type) => {
        return new Promise((resolve, reject) => {
            createPoint(vectorSource, ol.proj.fromLonLat(point), type)
                .then(resolve)
                .catch(error => console.log(error));
        })
    }

    const createPoint = (vectorSource, coordinates, type) => {
        return new Promise((resolve, reject) => {
            var name = "point";
            if (type != "station") {
                name = type;
            }
            route.points.push(ol.proj.toLonLat(coordinates));

            const point = new ol.geom.Point(coordinates);
            const feature = new ol.Feature({
                name: name,
                geometry: point
            });
            if (type == "busgoing") {
                feature.setStyle(styles.busgoing);
            } else if (type == "buscoming") {
                feature.setStyle(styles.buscoming);
            } else {
                feature.setStyle(styles.station);
            }
            vectorSource.addFeature(feature);
            route.features.push(feature);
            resolve();
        });
    };


    createMapOverlay()
        .then(overlay => {
            createMap(overlay)
                .then(map => {
                    createVectorLayer(map)
                        .then(vectorSource => {
                            return addClickHandlerForMap(map, overlay, vectorSource)
                                .then(addHandlerForSelect.bind(null, vectorSource));
                        })
                })
        });
})();