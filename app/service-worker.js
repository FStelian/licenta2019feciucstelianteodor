const cacheURLs = [
    "/css/back.css",
    "/css/forms.css",
    "/css/index.css",
    "/js/dash.js",
    "/js/dropdown.js",
    "/js/editProfile.js",
    "/js/forms.js",
    "/js/formtransition.js",
    "/js/sweetalert.js",
    "/js/index.js",
    "/js/instascan.min.js",
    "/js/menu.js",
    "/js/qr.js",
    "/js/qrcode.min.js",
    "/js/jquery-ui.min.js",
    "js/jquery-3.4.1.min.js",
    "/js/tickets.js",
    "/OpenLayers/ol.js",
    "/OpenLayers/ol.css",
    "/res/marker.png",
    "/res/logothree.svg",
    "/res/logothree.png",
    "/res/point.png",
    "/res/station.png",
    "/res/hoverpoint.png"
];

self.addEventListener("install", event => {
    event.waitUntil(
        caches.open("AWTC").then(cache => {
            const cachePromises = cacheURLs.map(item => {
                const request = new Request(item);
                fetch(request, {
                        credentials: "same-origin"
                    })
                    .then(response => {
                        if (response.status < 400) {
                            return cache.put(item, response);
                        }
                    })
            });
            return Promise.all(cachePromises);
        })
    );
});

self.addEventListener('fetch', function (event) {
    if (!event.request.url.includes('.page') && !event.request.url.includes('logout') && event.request.method == 'GET') {
        event.respondWith(
            caches.open('AWTC').then(function (cache) {
                return cache.match(event.request).then(function (response) {
                    var fetchPromise = fetch(event.request)
                        .then(function (networkResponse) {
                            cache.put(event.request, networkResponse.clone());
                            return networkResponse;
                        })
                    return response || fetchPromise;
                })
            })
        );
    }
});