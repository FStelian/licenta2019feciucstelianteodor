function scanner(e) {
    e.preventDefault();
    scannerVideo = document.createElement("video");
    scannerVideo.id = "preview";
    scannerDiv = document.createElement("div");
    scannerDiv.className = "msform msform-big msform-fixed";
    scannerDiv.appendChild(scannerVideo);
    document.body.appendChild(scannerDiv);

    let scanner = new Instascan.Scanner({
        video: document.getElementById('preview'),
        mirror: false
    });

    scanner.addListener('scan', (content) => {
        scanner.stop();

        fetch("/tickets/use", {
                method: "POST",
                credentials: "same-origin",
                body: JSON.stringify({
                    qr: content,
                    location: myLocation
                })
            })
            .then(result => result.json())
            .then(result => {

                if(result.response === "Ticket used") {
                    const ok = document.createElement("div")
                    scannerDiv.appendChild(ok);
                }
                setTimeout(() => {
                    scannerDiv.classList.add("msform-big-closing");
                    scannerDiv.classList.remove("msform-big");
                    setTimeout(() => {
                        scannerDiv.remove();
                    }, 250)
                }, 1000);
            })
    });
    Instascan.Camera.getCameras().then(function (cameras) {
        if (cameras.length > 0) {
            var cameraModePC = false;
            if (cameras.length > 1) {
                if (cameras[0].name == 'screen-capture-recorder' || cameras[1].name == 'screen-capture-recorder') {
                    cameraModePC = true;
                }
                if (cameraModePC == true) {
                    if (cameras[0].name != 'screen-capture-recorder') {
                        scanner.start(cameras[0]);
                    } else {
                        scanner.start(cameras[1]);
                    }
                } else {
                    scanner.start(cameras[2]);
                }
            } else {
                scanner.start(cameras[0]);
            }
        } else {
            console.error('No cameras found.');
        }
    }).catch(function (e) {
        console.error(e);
    });

    return false;
}
var myLocation = {
    longitude: "",
    latitude: ""
};

function genLocation(position) {
    location.latitude = position.coords.latitude;
    location.longitude = position.coords.longitude;
    myLocation.latitude = location.latitude;
    myLocation.longitude = location.longitude;
    return location;
}

function locate(e) {
    e.preventDefault();

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(genLocation);
    } else {
        alert("Location privileges not given");
    }
}
document.getElementById("use").addEventListener('click', locate, false);
document.getElementById("use").addEventListener('click', scanner, false);