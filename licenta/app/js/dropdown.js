function selectedItem(label, selectedItem) {
	var myLabel = document.getElementById(label);
	var myItem = document.getElementById(selectedItem);
	myLabel.innerHTML = myItem.innerHTML;
	if (label == "chart-type-label") {
		var dateLabel = document.getElementById("dropdown-date");
		var specLabel = document.getElementById("dropdown-spec");
		dateLabel.classList.add("hide");
		specLabel.classList.add("hide");
		dateLabel.classList.remove("hide");
		dateLabel.classList.add("fadeIn");
		setTimeout(function () {
			dateLabel.classList.remove("fadeIn");
		}, 950);
	}
	if (label == "chart-date-label") {
		var specLabel = document.getElementById("dropdown-spec");
		specLabel.classList.add("hide");
		specLabel.classList.remove("hide");
		specLabel.classList.add("fadeIn");
		setTimeout(function () {
			specLabel.classList.remove("fadeIn");
		}, 950);
	}
}