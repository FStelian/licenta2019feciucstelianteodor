function init() {
    var map = new ol.Map({
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()
            })
        ],
        target: "right",
        controls: ol.control.defaults({
            attributionOptions: {
                collapsible: false
            }
        }),
        view: new ol.View({
            center: ol.proj.fromLonLat([27.6014, 47.1555]),
            zoom: 13
        })
    });
}

if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    init();
} else {
    var width =
        window.innerWidth ||
        document.documentElement.clientWidth ||
        document.body.clientWidth;

    var height =
        window.innerHeight ||
        document.documentElement.clientHeight ||
        document.body.clientHeight;

    if (width >= 600 || height >= 600) {
        init();
    }
}