(() => {
    const routeURL = "/route";
    const routeGetByIdURL = "/routes/byId";
    const routeGetAllURL = "/routes"
    const table = document.getElementsByTagName("tbody")[0];
    const route = {
        qrElement: null,
        polylines: [],
        features: [],
        points: []
    };
    const styles = {
        route: new ol.style.Style({
            stroke: new ol.style.Stroke({
                width: 5,
                color: [40, 40, 40, 1]
            })
        }),
        station: new ol.style.Style({
            image: new ol.style.Icon({
                scale: 0.3,
                src: 'res/station.png'
            })
        }),
        point: new ol.style.Style({
            image: new ol.style.Icon({
                scale: 0.3,
                src: 'res/point.png'
            })
        }),
        hoverpoint: new ol.style.Style({
            image: new ol.style.Icon({
                scale: 0.3,
                src: 'res/hoverpoint.png'
            })
        })
    };

    const createMapOverlay = () => {
        return new Promise((resolve, reject) => {
            const mapDialog = document.getElementsByClassName("mapDialog")[0];
            const overlay = new ol.Overlay({
                element: mapDialog,
                autoPan: true,
                autoPanAnimation: {
                    duration: 250
                }
            });
            resolve(overlay);
        });
    };

    const createMap = overlay => {
        return new Promise((resolve, reject) => {
            const map = new ol.Map({
                layers: [
                    new ol.layer.Tile({
                        source: new ol.source.OSM()
                    })
                ],
                target: "right",
                controls: ol.control.defaults({
                    attributionOptions: {
                        collapsible: false
                    }
                }),
                view: new ol.View({
                    center: ol.proj.fromLonLat([27.6014, 47.1555]),
                    zoom: 13
                }),
                overlays: [overlay]
            });

            resolve(map);
        });
    };

    const createVectorLayer = map => {
        return new Promise((resolve, reject) => {
            const vectorSource = new ol.source.Vector({});
            const vectorLayer = new ol.layer.Vector({
                source: vectorSource
            });
            map.addLayer(vectorLayer);

            resolve(vectorSource);
        });
    };

    const addClickFeatureForOverlayButton = (overlay, vectorSource) => {
        return new Promise((resolve, reject) => {
            const dialog = document.getElementsByClassName("mapDialog")[0];
            const idElement = dialog.getElementsByTagName("i")[0];
            const button = dialog.getElementsByTagName("button")[0];
            button.addEventListener("click", event => {
                dialog.classList.remove("hide");
                deleteAction(vectorSource, idElement, event);
                overlay.setPosition(undefined);
            });
            resolve();
        });
    };

    const addSelectFeatureForOverlaySelect = (overlay, vectorSource) => {
        return new Promise((resolve, reject) => {
            const dialog = document.getElementsByClassName("mapDialog")[0];
            const idElement = dialog.getElementsByTagName("i")[0];
            const select = dialog.getElementsByTagName("select")[0];
            select.addEventListener("change", event => {
                selectAction(vectorSource, idElement, event);
            });
            resolve();
        });
    };

    const addClickFeature = (map, overlay, vectorSource) => {
        map.on("click", event => {
            const coordinates = ol.proj.toLonLat(event.coordinate);
            let featureExists = false;

            overlay.setPosition(undefined);
            map.forEachFeatureAtPixel(event.pixel, (feature, layer) => {
                if (feature.N.name && feature.N.name === "point") {
                    featureExists = true;
                    dialogHandler(feature, overlay, event);
                }
            });

            if (!featureExists) {
                mapFlow(vectorSource, coordinates);
            }
        });
    };

    const mapFlow = (vectorSource, point, type) => {
        return new Promise((resolve, reject) => {
            createPoint(vectorSource, ol.proj.fromLonLat(point), type)
                .then(createTableRow.bind(null, vectorSource, point))
                .then(() => {
                    if (enoughPointsToCreateRoute(route.points)) {
                        return fetchRoute(route.points).then(createRoute.bind(null, vectorSource));
                    }
                })
                .then(resolve)
                .catch(error => console.log(error));
        })
    }

    const dialogHandler = (feature, overlay, event) => {
        const coordinate = event.coordinate;
        const dialog = document.getElementsByClassName("mapDialog")[0];
        dialog.classList.remove("hide");
        overlay.setPosition(coordinate);

        const idElement = dialog.getElementsByTagName("i")[0];
        idElement.innerHTML = route.features.findIndex(value => value === feature) + 1;
        const select = dialog.getElementsByTagName("select")[0];
        select.value = "station";
    };

    const deleteAction = (vectorSource, idElement, event) => {
        let parentRow = null;
        const id = idElement.innerHTML;

        if (idElement.tagName === "I") {
            const elements = document.getElementsByTagName("td");
            for (let node of elements) {
                if (node.innerHTML === idElement.innerHTML) {
                    parentRow = node.parentElement;
                    break;
                }
            }
        } else {
            parentRow = idElement.parentNode;
        }

        route.points.splice(id - 1, 1);
        vectorSource.removeFeature(route.features[id - 1]);
        route.features.splice(id - 1, 1);

        while (route.polylines.length > 0) {
            vectorSource.removeFeature(route.polylines[0]);
            route.polylines.shift();
        }

        const nodes = parentRow.parentNode.getElementsByTagName("tr");
        for (const node of nodes) {
            let otherId = node.getElementsByTagName("td")[0];
            if (Number(otherId.innerHTML) > id) {
                otherId.innerHTML = Number(otherId.innerHTML) - 1;
            }
        }

        parentRow.remove();

        if (enoughPointsToCreateRoute(route.points)) {
            return fetchRoute(route.points).then(createRoute.bind(null, vectorSource));
        }
    };

    const selectAction = (vectorSource, idElement, event) => {
        const mapDialog = document.getElementsByClassName("mapDialog")[0];
        route.points[idElement.innerHTML - 1].type = event.target.value;
        route.features[idElement.innerHTML - 1].setStyle(styles[event.target.value]);
        mapDialog.classList.add("hide");
    }

    const createTableRow = (vectorSource, coordinates) => {
        const row = document.createElement("tr");
        const id = document.createElement("td");
        const lon = document.createElement("td");
        const lat = document.createElement("td");
        const action = document.createElement("td");
        const button = document.createElement("button");

        id.classList.add("index");
        id.innerHTML = route.points.length;
        lon.innerHTML = coordinates[0].toFixed(8);
        lat.innerHTML = coordinates[1].toFixed(8);
        button.innerHTML = "Şterge";

        row.classList.add("hoverable");
        
        row.setAttribute("data-style", "station");

        button.addEventListener("click", deleteAction.bind(null, vectorSource, id));
        
        row.appendChild(id);
        row.appendChild(lon);
        row.appendChild(lat);
        row.appendChild(action);
        action.appendChild(button);

        table.appendChild(row);
    };

    const createPoint = (vectorSource, coordinates, type) => {
        return new Promise((resolve, reject) => {
            const tdIndex = $("td.index");
            const orderNo = tdIndex.length + 1;

            route.points.push({
                type: type || "station",
                coordinates: ol.proj.toLonLat(coordinates),
                order: orderNo
            });

            const point = new ol.geom.Point(coordinates);
            const feature = new ol.Feature({
                name: "point",
                geometry: point
            });
            if (type && type === "point") {
                feature.setStyle(styles.point);
            } else {
                feature.setStyle(styles.station);
            }
            vectorSource.addFeature(feature);
            route.features.push(feature);
            resolve();
        });
    };

    const enoughPointsToCreateRoute = points => {
        return points.length > 1;
    };

    const fetchRoute = points => {
        let url = routeURL;

        return fetch(url, {
                method: "POST",
                credentials: "same-origin",
                body: JSON.stringify({
                    points
                })
            })
            .then(data => data.json())
            .then(json => {
                if (!json.code || json.code !== "Ok") {
                    throw json.message;
                } else {
                    return json.routes[0].geometry;
                }
            });
    };

    const createRoute = (vectorSource, polyline) => {
        return new Promise((resolve, reject) => {
            const routeGeometry = new ol.format.Polyline({
                factor: 1e5
            }).readGeometry(polyline, {
                dataProjection: "EPSG:4326",
                featureProjection: "EPSG:3857"
            });

            const feature = new ol.Feature({
                type: "route",
                geometry: routeGeometry
            });

            feature.setStyle(styles.route);
            vectorSource.addFeature(feature);
            route.polylines.push(feature);
            resolve();
        });
    };

    const addSubmitHandlerForAddForm = (overlay) => {
        const nodes = document.getElementsByTagName("form");
        let form = null;

        for (const node of nodes) {
            if (/\/routes$/.test(node.action) && node.id === "POST") {
                form = node;
                break;
            }
        }

        form.addEventListener("submit", submitAddForm.bind(null, overlay));
    }

    const submitAddForm = (overlay, event) => {
        event.preventDefault();
        var number = event.target.getElementsByTagName("input")[0].value;
        overlay.setPosition(undefined);
        fetch(event.target.action, {
                method: event.target.id,
                credentials: "same-origin",
                body: JSON.stringify({
                    number: number,
                    points: route.points
                })
            })
            .then(data => data.json())
            .then(body => {
                if (body.error) {
                    Swal.fire({
                        type: 'error',
                        title: body.message,
                        showConfirmButton: false,
                        timer: 1200
                      }); 
                } else {
                    document.getElementById("route-id").value = body.ruta
                    let selectRuta = document.getElementById("select-ruta");
                    let optionSelectRuta = document.createElement('option');
                    optionSelectRuta.value = body.ruta;
                    optionSelectRuta.innerHTML = number;
                    optionSelectRuta.selected = 'selected';
                    selectRuta.appendChild(optionSelectRuta);
                    let vehiculeRutaSelect = document.getElementById("vehicule-ruta");
                    vehiculeRutaSelect.innerHTML = "";
                    let option = document.createElement('option');
                    option.value = body.vehicul;
                    option.innerHTML = body.vehicul;
                    vehiculeRutaSelect.appendChild(option);
                    document.getElementById("div-vehicule-ruta").classList.remove("hide");
                    Swal.fire({
                        type: 'success',
                        title: body.message,
                        showConfirmButton: false,
                        timer: 1200
                      });
                    if (body.qr) {
                        var qr = document.getElementById("qr");
                        qr.classList.remove("hide");
                        route.qrElement.makeCode(body.qr);
                        var qrImg = qr.getElementsByTagName("img")[0];
                        qrImg.style = "";
                        qrImg.classList.add("qr-img");
                    }
                }
            });
        return false;
    }

    const addSubmitHandlerForEditForm = (map, vectorSource, overlay) => {
        const nodes = document.getElementsByTagName("form");
        let form = null;
        let select = null;

        for (const node of nodes) {
            if (/\/routes$/.test(node.action) && node.id === "PUT") {
                form = node;
                break;
            }
        }
        select = form.getElementsByTagName("select")[0];

        form.addEventListener("submit", submitEditForm.bind(null, map, vectorSource, overlay, form));
        select.addEventListener("change", editFormSelectHandler.bind(null, map, vectorSource, overlay, form));
    }

    const submitEditForm = (map, vectorSource, overlay, form, event) => {
        event.preventDefault();

        overlay.setPosition(undefined);
        fetch(event.target.action, {
                method: event.target.id,
                credentials: "same-origin",
                body: JSON.stringify({
                    number: form.getElementsByTagName("select")[0].value,
                    points: route.points
                })
            })
            .then(data => data.json())
            .then(response => {
                if (response.qr) {
                    Swal.fire({
                        type: 'success',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 1200
                    });
                    let vehiculeRutaSelect = document.getElementById("vehicule-ruta");
                    vehiculeRutaSelect.innerHTML = "";
                    for (const vehiculId of response.vehiculeIds) {
                        let option = document.createElement('option');
                        option.value = vehiculId;
                        option.innerHTML = vehiculId;
                        vehiculeRutaSelect.appendChild(option);
                    }
                    var qr = document.getElementById("qr");
                    qr.classList.remove("hide");
                    route.qrElement.makeCode(response.qr);
                    var qrImg = qr.getElementsByTagName("img")[0];
                    qrImg.style = "";
                    qrImg.classList.add("qr-img");
                }
            })

        return false;
    }

    const addBodyPoints = (vectorSource, points) => {
        if (points.length > 0) {
            mapFlow(vectorSource, points[0].coordinates, points[0].type)
                .then(() => {
                    points.shift();
                    addBodyPoints(vectorSource, points);
                })
        }
    }

    const editFormSelectHandler = (map, vectorSource, overlay, form, event) => {
        overlay.setPosition(undefined);
        if (event.target.value !== "") {
            fetch(routeGetByIdURL, {
                    method: "POST",
                    credentials: "same-origin",
                    body: JSON.stringify({
                        id: event.target.value
                    })
                })
                .then(data => data.json())
                .then(body => {
                    $('.hoverable').remove();
                    cleanUp(vectorSource);
                    addBodyPoints(vectorSource, body.points);
                    let vehiculeRutaSelect = document.getElementById("vehicule-ruta");
                    vehiculeRutaSelect.innerHTML = "";
                    for (const vehiculId of body.vehiculeIds) {
                        let option = document.createElement('option');
                        option.value = vehiculId;
                        option.innerHTML = vehiculId;
                        vehiculeRutaSelect.appendChild(option);
                    }
                    document.getElementById("route-id").value = event.target.value;
                    document.getElementById("div-vehicule-ruta").classList.remove("hide");
                    if (body.qr) {
                        var qr = document.getElementById("qr");
                        qr.classList.remove("hide");
                        route.qrElement.makeCode(body.qr);
                        var qrImg = qr.getElementsByTagName("img")[0];
                        qrImg.style = "";
                        qrImg.classList.add("qr-img");
                    }
                })
        }
    }

    let getUpdatedRoutes = (form) => {
        const select = form.getElementsByTagName("select")[0];

        fetch(routeGetAllURL, {
                credentials: "same-origin"
            })
            .then(data => data.json())
            .then(body => {
                while (select.firstChild) {
                    select.removeChild(select.firstChild);
                }
                select.appendChild(document.createElement("option"));
                for (const route of body.routes) {
                    const option = document.createElement("option");
                    option.innerHTML = route;
                    select.appendChild(option);
                }
            })
    }

    const cleanUp = (vectorSource) => {
        while (route.features.length > 0) {
            vectorSource.removeFeature(route.features[0]);
            route.features.shift();
        }

        while (route.polylines.length > 0) {
            vectorSource.removeFeature(route.polylines[0]);
            route.polylines.shift();
        }

        route.points = [];
        route.features = [];
        route.polylines = [];

        const tableRows = table.getElementsByTagName("tr");
        for (let i = 1; i < tableRows.length; i++) {
            tableRows[i].remove();
        }
    }

    route.qrElement = new QRCode(document.getElementById("qr"), {
        text: "",
        width: 256,
        height: 256,
        colorDark: "#000000",
        colorLight: "#ffffff",
        correctLevel: QRCode.CorrectLevel.H
    });

    const fixHelperModified = function(e, tr) {
        let $originals = tr.children();
        let $helper = tr.clone();
        $helper.children().each(function(index) {
            $(this).width($originals.eq(index).width())
        });
        return $helper;
    };

    updateIndex = function(vectorSource, e, ui) {
        let newRoute = {
            features: [],
            points: []
        };

        for (let i = 0, len = route.points.length; i < len; i++) {
            newRoute.points.push({});
            newRoute.features.push({});
        }

        $('td.index', ui.item.parent()).each(function (i) {
            let oldId = $(this).html() - 1;
            let newId = i;
            newRoute.points[newId]   = route.points[oldId];
            newRoute.features[newId] = route.features[oldId];

            $(this).html(i + 1);
        });

        route.points   = newRoute.points;
        route.features = newRoute.features;
        while (route.polylines.length > 0) {
            vectorSource.removeFeature(route.polylines[0]);
            route.polylines.shift();
        }
        fetchRoute(route.points).then(createRoute.bind(null, vectorSource));
    };


    const addSortableProperty = (vectorSource) => {
        $("#sort tbody").sortable({
            helper: fixHelperModified,
            stop: updateIndex.bind(null, vectorSource)
        }).disableSelection();
    }

    $(document).on("mouseenter", "tr.hoverable", function() {
        const tdIndex = $(this).find("td.index");
        const id = tdIndex[0].innerHTML - 1;
        route.features[id].setStyle(styles.hoverpoint);
    });
    
    $(document).on("mouseleave", "tr.hoverable", function() {
        const tdIndex = $(this).find("td.index");
        const id = tdIndex[0].innerHTML - 1;
        const style = $(this).data("style");
        route.features[id].setStyle(styles[style]);
    });

    $("#vehicule-ruta").on("change", function(e) {
        const url = $(this).data("url") + $("#vehicule-ruta option:selected").val();
        fetch(url, {
            method: "GET",
            credentials: "same-origin"
        })
        .then(data => data.json())
        .then(body => {
            if (body.length > 0) {
                var qr = document.getElementById("qr");
                qr.classList.remove("hide");
                route.qrElement.makeCode(body[0].qr);
                var qrImg = qr.getElementsByTagName("img")[0];
                qrImg.style = "";
                qrImg.classList.add("qr-img");
            }
        })
    });

    $("#adauga-vehicule-ruta").on("click", function(e) {
        e.preventDefault();
        const url = $(this).data("url");
        fetch(url, {
            method: "POST",
            credentials: "same-origin",
            body: JSON.stringify({
                id: $("#route-id").val()
            })
        })
        .then(data => data.json())
        .then(body => {
            Swal.fire({
                type: 'success',
                title: body.message,
                showConfirmButton: false,
                timer: 1200
              });
            let vehiculeRutaSelect = document.getElementById("vehicule-ruta");
            let option = document.createElement('option');
            option.value = body.id;
            option.innerHTML = body.id;
            option.selected = 'selected';
            vehiculeRutaSelect.appendChild(option);
            var qr = document.getElementById("qr");
            qr.classList.remove("hide");
            route.qrElement.makeCode(body.qr);
            var qrImg = qr.getElementsByTagName("img")[0];
            qrImg.style = "";
            qrImg.classList.add("qr-img");
        })
    });

    $("#sterge-vehicule-ruta").on("click", function(e) {
        e.preventDefault();
        const url = $(this).data("url");
        let vehiculeRutaSelect = document.getElementById("vehicule-ruta");
        fetch(url, {
            method: "DELETE",
            credentials: "same-origin",
            body: JSON.stringify({
                id: vehiculeRutaSelect.options[vehiculeRutaSelect.selectedIndex].text
            })
        })
        .then(data => data.json())
        .then(body => {
            if (body.success == true) {
                Swal.fire({
                    type: 'success',
                    title: body.message,
                    showConfirmButton: false,
                    timer: 1200
                  });
                vehiculeRutaSelect.remove(vehiculeRutaSelect.selectedIndex);
                const url = $("#vehicule-ruta").data("url") + $("#vehicule-ruta option:selected").val();
                fetch(url, {
                    method: "GET",
                    credentials: "same-origin"
                })
                .then(data => data.json())
                .then(body => {
                    if (body.length > 0) {
                        var qr = document.getElementById("qr");
                        qr.classList.remove("hide");
                        route.qrElement.makeCode(body[0].qr);
                        var qrImg = qr.getElementsByTagName("img")[0];
                        qrImg.style = "";
                        qrImg.classList.add("qr-img");
                    }
                });
            } else {
                Swal.fire({
                    type: 'error',
                    title: body.message,
                    showConfirmButton: false,
                    timer: 1200
                  });
            }
        })
    });

    createMapOverlay()
        .then(overlay => createMap(overlay)
            .then(map => createVectorLayer(map)
                .then(vectorSource => addClickFeatureForOverlayButton(overlay, vectorSource)
                    .then(addSelectFeatureForOverlaySelect.bind(null, overlay, vectorSource))
                    .then(addClickFeature.bind(null, map, overlay, vectorSource))
                    .then(addSubmitHandlerForEditForm.bind(null, map, vectorSource, overlay))
                    .then(addSubmitHandlerForAddForm.bind(null, overlay))
                    .then(addSortableProperty.bind(null, vectorSource))
                )
            )
        );
})();