var ctx = document.getElementById("largeChart").getContext('2d');
const url ="/statistics";
const borderAlpha = 1;
const backgroundAlpha = 0.2;
var payload;
var myChart;

const getStats = () => {
    return new Promise((resolve, refuse) => {
        fetch(url , {
            method: "GET",
            mode: "cors",
            credentials: "same-origin",
            headers: {
                'Accept': 'application/json'
            }
        })
            .then((response) => {
                return response.json();
            })
            .then((result) => {
                resolve(result);
            })
            .catch((error) => {
                refuse(error);
            });
    });
};

function getRGBAString(r, g, b, a) {
    return "rgba(" + r + ", " + g + ", " + b + ", " + a + ")"; 
}

function makeChart (type){
    if(type === "first" || type === "users") {
        if (myChart) {
            myChart.destroy();
        }
        let labels = payload.chart.users.map((user) => {
            return user.username;
        });
        let data = payload.chart.users.map((user) => {
            return user.count;
        });
        let backgroundColor = [];
        let borderColor = [];
        for (let i = 0; i < labels.length; i++) {
            let r = Math.floor(Math.random() * 256);
            let g = Math.floor(Math.random() * 256);
            let b = Math.floor(Math.random() * 256);
            backgroundColor.push(getRGBAString(r, g, b, backgroundAlpha));
            borderColor.push(getRGBAString(r, g, b, borderAlpha));
        }
        myChart = new Chart(ctx, {
            type: 'horizontalBar',
            data: {
                labels: labels,
                datasets: [{
                    label: 'Compostari',
                    data: data,
                    backgroundColor: backgroundColor,
                    borderColor: borderColor,
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    xAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
    } else {
        if (myChart) {
            myChart.destroy();
        }
        let labels = payload.chart.routes.map((route) => {
            return route.nume;
        });
        let data = payload.chart.routes.map((route) => {
            return route.count;
        });
        let backgroundColor = [];
        let borderColor = [];
        for (let i = 0; i < labels.length; i++) {
            let r = Math.floor(Math.random() * 256);
            let g = Math.floor(Math.random() * 256);
            let b = Math.floor(Math.random() * 256);
            backgroundColor.push(getRGBAString(r, g, b, backgroundAlpha));
            borderColor.push(getRGBAString(r, g, b, borderAlpha));
        }
        myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: labels,
            datasets: [{
                label: 'Utilizatori',
                data: data,
                backgroundColor: backgroundColor,
                borderColor: borderColor,
                borderWidth: 1
            }]
        },
        options: {
            title: {
                display: true,
                text: 'Cele mai folosite rute'
            },
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
        });
    }
}

getStats().then((result) => {
    payload = result.response;
    makeChart("first");
});
