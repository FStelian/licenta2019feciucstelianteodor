processForm = e => {
    e.preventDefault();
    const form = e.target;
    let data = {};

    const inputs = form.getElementsByTagName("input");
    for (let input of inputs) {
        if (input.type !== "submit") {
            data[input.name] = input.value;
        }
    }

    fetch(form.action, {
            method: form.id,
            credentials: "same-origin",
            body: JSON.stringify(data)
        })
        .then(response => response.json())
        .then(response => {
            if (response.status < 400) {
                Swal.fire({
                    type: 'success',
                    title: response.message,
                    showConfirmButton: false,
                    timer: 1200
                }); 
            } else {
                Swal.fire({
                    type: 'error',
                    title: response.message,
                    showConfirmButton: false,
                    timer: 1200
                }); 
            }
        });

    return false;
};

const formSubmitListener = item => {
    item.addEventListener(
        "submit",
        function (event) {
            processForm(event);
        },
        false
    );
};

const forms = document.getElementsByTagName("form");
for (let item of forms) {
    if (/\/users$/.test(item.action) && item.id === "PUT") {
        formSubmitListener(item);
    }
}