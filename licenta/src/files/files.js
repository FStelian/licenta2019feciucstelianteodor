module.exports = (() => {
    const fs = require("fs");
    const mime = require("./mime.js");
    const authedFiles = require("./fileList");
    const dynamicejs = require("../dynamicejs/dynamicvalues");
    const files = {
        "/index.page": dynamicejs.serveIndex,
        "/tickets.page": dynamicejs.serveTickets,
        "/dashboard.page": dynamicejs.serveDashboard,
        "/admindashboard.page": dynamicejs.serveAdminDashboard,
        "/profile.page": dynamicejs.serveProfile,
        "/contact.page": dynamicejs.serveContact,
        "/manageroutes.page": dynamicejs.serveManageRoutes,
        "/driverdashboard.page": dynamicejs.serveDriverDashboard
    };

    const staticFiles = () => {
        let routes = [];

        authedFiles.notAuthed.forEach(item => {
            routes.push({
                admin: false,
                authed: false,
                driver: false,
                method: "GET",
                url: item,
                fn: serveFile
            });
        });
        authedFiles.authed.forEach(item => {
            routes.push({
                admin: false,
                authed: true,
                driver: false,
                method: "GET",
                url: item,
                fn: serveFile
            });
        });
        authedFiles.admin.forEach(item => {
            routes.push({
                admin: true,
                authed: true,
                driver: false,
                method: "GET",
                url: item,
                fn: serveFile
            });
        });
        authedFiles.driver.forEach(item => {
            routes.push({
                admin: false,
                authed: true,
                driver: true,
                method: "GET",
                url: item,
                fn: serveFile
            });
        });

        routes.push({
            authed: false,
            method: "GET",
            url: "/",
            fn: serveFile
        });

        return routes;
    };

    const serveFile = (request, response) => {
        if (request.url == "/") {
            request.url = "/index.page";
        }
        if (/.page$/.test(request.url)) {
            if (files[request.url]) {
                files[request.url](request, response);
            }
        } else {
            const stream = fs.createReadStream("./app" + request.url);
            stream.on("open", () => {
                response.writeHead(200, {
                    "Content-type": mime.getMime(request.url)
                });
                stream.pipe(response);
            });
            stream.on("error", () => {
                response.writeHead(404, {
                    "Content-type": "text/html"
                });
                // stream.pipe(response);
                response.end();
            });
        }
    };

    return {
        staticFiles,
        serveFile
    };
})();
