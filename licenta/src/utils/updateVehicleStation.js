module.exports = (() => {
    const db = require("../db/database");
    const { getBody, parseBody } = require("../helpers/helpers");
    const http = require("http");

    const defaultPath = "/route/v1/driving/";
    let options = {
        host: "0.0.0.0",
        port: 5000,
        path: "/route/v1/driving/"
    };

    const updateVehicleStation = () => {
        db.doQuery("SELECT * FROM vehicule_transport").then(vehiculeTransport => {
            vehiculeTransport.forEach(vehicul => {
                var directieInainte = vehicul["directie_inainte"];
                var queryStr = "SELECT * FROM puncte WHERE id_ruta = ? and ordine >= (SELECT ordine FROM puncte WHERE id = ?) order by ordine asc";
                if (directieInainte == 0) {
                    queryStr = "SELECT * FROM puncte WHERE id_ruta = ? and ordine <= (SELECT ordine FROM puncte WHERE id = ?) order by ordine desc";
                }
                db.doQuery(queryStr, [vehicul["id_ruta"], vehicul["statie_noua"]]).then(puncte => {
                    requestVehicleRouteData(vehicul, puncte)
                        .then(parseBody)
                        .then(body => {
                            var lastUpdate = new Date(vehicul["data_update"]);
                            var currentDate = new Date();
                            var iter = 1;
                            var statieNoua  = vehicul["statie_noua"];

                            while (lastUpdate < currentDate && iter < puncte.length) {
                                var auxDate = new Date(lastUpdate)
                                auxDate.setSeconds(auxDate.getSeconds() + body.routes[0].legs[iter - 1].duration);
                                if (auxDate < currentDate) {
                                    statieNoua = puncte[iter]["id"];
                                    lastUpdate = new Date(auxDate);
                                }
                                iter++;
                            }

                            db.doQuery("UPDATE vehicule_transport SET statie_noua = ?, data_update = ? WHERE id = ?", [statieNoua, lastUpdate, vehicul.id]);
                        });
                });
            });
            console.log(new Date());
            setTimeout(updateVehicleStation, 120000);
        });
    };

    const requestVehicleRouteData = (vehicul, puncte) => {
        return new Promise((resolve, reject) => {

            let path = defaultPath;

            puncte.forEach(punct => {
                path += `${punct["coordonate_long"]},${punct["coordonate_lat"]};`;
            });
            
            path = path.substr(0, path.length - 1);
            options.path = path;

            let data = "";

            const req = http.request(options, res => {
                res.on("data", chunk => {
                    data += chunk;
                });
                res.on("end", () => {
                    let parsedData = JSON.parse(data);
                    resolve(JSON.stringify(parsedData))
                });
                res.on("error", error => reject(error));
            });

            req.end();
        });
    };

    return {
        updateVehicleStation
    };
})();