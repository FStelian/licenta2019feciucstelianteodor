module.exports = (() => {
	const { getCookie } = require("../cookie");
    const mime = require("../files/mime.js");
	const db = require("../db/database");
	var ejs = require("ejs");
	
    const serveTickets = (request, response) => 
	{
		const token = getCookie(request["headers"], "token");
		var rutaPopulara = "Nu exista nicio ruta folosita pana in acest moment";
        db.doQuery(`SELECT id_utilizator FROM tokens WHERE token=?`, [token]).then(tokens => {
            db.doQuery(`SELECT * FROM utilizatori WHERE id=?`, [tokens[0]["id_utilizator"]]).then(users => {
				db.doQuery(`SELECT * FROM rute WHERE id = (SELECT id_ruta FROM istoric_bilete WHERE id_utilizator = ? GROUP BY id_ruta 
				ORDER BY count(id_ruta) DESC LIMIT 1)`,[tokens[0]["id_utilizator"]]).then(ruta_populara => {
					db.doQuery(`SELECT nume, data FROM rute r JOIN (SELECT data , id_ruta FROM istoric_bilete WHERE id_utilizator = ?) i ON r.id = i.id_ruta`,
					[tokens[0]["id_utilizator"]]).then(istoric_rute => {
						if(ruta_populara.length > 0){
							rutaPopulara = ruta_populara[0].nume;
						}
						ejs.renderFile('./app' + '/tickets.ejs', {ejsNrBilete: users[0].numar_bilete, ejsRutaPopulara: rutaPopulara,
						ejsIstoricRute: istoric_rute, isAdmin: users[0].nivel_acces}, function(err, data){
							response.writeHeader(200, {"Content-Type": mime.getMime('/tickets.ejs')});
							response.write(data);
							response.end();
						});
					});
				});
            });
        });
	};
	
	const serveIndex = (request, response) =>
	{
		const token = getCookie(request["headers"], "token");
		db.doQuery(`SELECT id_utilizator FROM tokens WHERE token=?`, [token]).then(tokens => {
			if (tokens && token.length > 0) {
				response.writeHeader(302, {"Content-Type": "text/html", "Location": "/dashboard.page"});
				response.end();
			} else {
				ejs.renderFile('./app' + '/index.ejs', function(err, data){
					response.writeHeader(200, {"Content-Type": "text/html"});
					response.write(data);
					response.end();
				});
			}
		});
	};
	
	const serveContact = (request, response) =>
	{
		const token = getCookie(request["headers"], "token");
		db.doQuery(`SELECT id_utilizator FROM tokens WHERE token=?`, [token]).then(tokens => {
            db.doQuery(`SELECT * FROM utilizatori WHERE id=?`, [tokens[0]["id_utilizator"]]).then(users => {
				ejs.renderFile('./app' + '/contact.ejs', {isAdmin: users[0].nivel_acces}, function(err, data){
					response.writeHeader(200, {"Content-Type": "text/html"});
					response.write(data);
					response.end();
				});
			});
		});
	};
	
	const serveProfile = (request, response) =>
	{
		const token = getCookie(request["headers"], "token");
		db.doQuery(`SELECT * FROM tokens WHERE token=?`, [token]).then(tokens => {
			db.doQuery(`SELECT * FROM utilizatori WHERE id=?`, [tokens[0]["id_utilizator"]]).then(users => {
				ejs.renderFile('./app' + '/profile.ejs',{nume: users[0].nume, prenume: users[0].prenume,
				email: users[0].email, nr_telefon: users[0].nr_telefon, editPrenume: users[0].prenume,
				editNume: users[0].nume, editEmail: users[0].email, editAge: users[0].varsta, editPhone: users[0].nr_telefon, isAdmin: users[0].nivel_acces},function(err, data){
					response.writeHeader(200, {"Content-Type": "text/html"});
					response.write(data);
					response.end();
				});
			});
		});
	};
	
	const serveAdminDashboard = (request, response) =>
	{
		const token = getCookie(request["headers"], "token");
		db.doQuery(`SELECT id_utilizator FROM tokens WHERE token=?`, [token]).then(tokens => {
            db.doQuery(`SELECT * FROM utilizatori WHERE id=?`, [tokens[0]["id_utilizator"]]).then(users => {
				db.doQuery(`SELECT COUNT(id) AS "nr_clienti" FROM utilizatori WHERE nivel_acces=0`, []).then(count_utilizatori => {
					db.doQuery(`SELECT COUNT(id) AS "nr_admini" FROM utilizatori WHERE nivel_acces=1`, []).then(count_admini => {
						db.doQuery(`SELECT COUNT(id) AS "nr_soferi" FROM utilizatori WHERE nivel_acces=2`, []).then(count_soferi => {
							ejs.renderFile('./app' + '/admindashboard.ejs', {nr_clienti: count_utilizatori[0].nr_clienti,
							nr_admini: count_admini[0].nr_admini, nr_soferi: count_soferi[0].nr_soferi, isAdmin: users[0].nivel_acces}, function(err, data){
								response.writeHeader(200, {"Content-Type": "text/html"});
								response.write(data);
								response.end();
							});
						});
					});
				});
			});
		});
	};
	
	const serveDashboard = (request, response) =>
	{
		const token = getCookie(request["headers"], "token");
		 db.doQuery(`SELECT id_utilizator FROM tokens WHERE token=?`, [token]).then(tokens => {
            db.doQuery(`SELECT * FROM utilizatori WHERE id=?`, [tokens[0]["id_utilizator"]]).then(users => {
				db.doQuery(`SELECT * FROM rute`, []).then(rute => {
					ejs.renderFile('./app' + '/dashboard.ejs', {isAdmin: users[0].nivel_acces, routes:rute}, function(err, data){
						response.writeHeader(200, {"Content-Type": "text/html"});
						response.write(data);
						response.end();
					});
				});
			});
		});
	};

	const serveDriverDashboard = (request, response) =>
	{
		const token = getCookie(request["headers"], "token");
		 db.doQuery(`SELECT id_utilizator FROM tokens WHERE token=?`, [token]).then(tokens => {
            db.doQuery(`SELECT * FROM utilizatori WHERE id=?`, [tokens[0]["id_utilizator"]]).then(users => {
				ejs.renderFile('./app' + '/driverdashboard.ejs', {isAdmin: users[0].nivel_acces}, function(err, data){
					response.writeHeader(200, {"Content-Type": "text/html"});
					response.write(data);
					response.end();
				});
			});
		});
	};
	
	const serveManageRoutes = (request, response) =>
	{
		const token = getCookie(request["headers"], "token");
		 db.doQuery(`SELECT id_utilizator FROM tokens WHERE token=?`, [token]).then(tokens => {
            db.doQuery(`SELECT * FROM utilizatori WHERE id=?`, [tokens[0]["id_utilizator"]]).then(users => {
                db.doQuery(`SELECT * FROM rute`).then(routes => {
                    ejs.renderFile('./app' + '/manageroutes.ejs', {isAdmin: users[0].nivel_acces,routes:routes}, function(err, data){
                        response.writeHeader(200, {"Content-Type": "text/html"});
                        response.write(data);
                        response.end();
                    });
                });
			});
		});
	};
	
	return { 
	serveTickets,
	serveIndex, 
	serveContact,
	serveProfile,
	serveAdminDashboard,
	serveDashboard,
	serveDriverDashboard,
	serveManageRoutes};
})();

