module.exports = (() => {
    const db = require("../db/database");
    const { getCookie } = require("../cookie");
    const { writeResponse } = require("../router");
    const {
        getBody,
        parseBody
    } = require("../helpers/helpers");

    const contact = (request, response) => {
        var id;
        var email;
        let { headers } = request;
        const token = getCookie(headers, "token");

        getBody(request)
            .then(parseBody)
            .then(body => {

            db.doQuery(`SELECT * FROM tokens WHERE token=?`, [token]
            ).then(result => {
                if (result.length === 1 && body.payload["subject"].length > 0 && body.payload["message"].length > 0) {
                    id = result[0]["id_utilizator"];
                    return db
                        .doQuery(`SELECT email from utilizatori where id= ?`,
                            [id]
                        )
                        .then((result) => {
                            if(result.length === 1){
                                email = result[0]["email"];
                                db.doQuery('INSERT INTO `inbox`(`subject`, `message`, `timestamp`, `id_utilizator`, `email`) VALUES (?, ?, CURRENT_TIMESTAMP() , ?,?)',
                                    [body.payload["subject"], body.payload["message"], id, email]).catch(err =>{
                                    console.log(err);
                                });

                                writeResponse(
                                    {
                                        header: {
                                            status: 200,
                                            reason: "OK",
                                        },
                                        body: JSON.stringify({
                                            status: 200,
                                            message: "Mesaj trimis"
                                        })
                                    },
                                    response
                                );
                            }

                        });
                } else {
                    writeResponse(
                        {
                            header: {
                                status: 400,
                                reason: "Bad request"
                            },
                            body: JSON.stringify({
                                status: 400,
                                message: "Mesajul nu s-a putut tirmite"
                            })
                        },
                        response
                    );
                }
            })
                .catch(error => {
                    writeResponse(
                        {
                            header: {
                                status: 500,
                                reason: "Internal server error"
                            },
                            body: JSON.stringify({
                                status: 500,
                                message: "Internal server error"
                            })
                        },
                        response
                    );
                });
        });
    };

    return contact;
})();

//example of object expected from frontend:
//all the other info is generated and inserted into the table (timestamp, id_user, email)
// {
//     "subject" : "Postman test",
//     "message" : "blabla"
// }