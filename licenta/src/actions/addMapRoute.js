module.exports = (() => {
    const db = require("../db/database");
    const genToken = require("../utils/genToken");
    const {
        getBody,
        parseBody
    } = require("../helpers/helpers");
    const {
        writeResponse
    } = require("../router");

    const addMapRoute = (request, response) => {
        getBody(request)
            .then(parseBody)
            .then(body => {
                const token = genToken(100);
                body.token = token;
                return validateBody(body)
                    .then(insertIntoDatabase.bind(null, body, response))
                    .then(sendResponse.bind(null, response, undefined, {
                        status: 200,
                        message: "Rută inserată",
                        qr: body.token,
                        vehicul: vehicul,
                        ruta: ruta
                    }))
            })
            .catch(sendError.bind(null, response))
    }

    const validateBody = body => {
        return new Promise((resolve, reject) => {
            body.points.forEach(item => {
                if ((item.type !== "station" &&
                    item.type !== "point") ||
                    !Array.isArray(item.coordinates) ||
                    typeof item.coordinates[0] !== "number" ||
                    typeof item.coordinates[1] !== "number") {
                    reject({
                        status: 400,
                        message: "Bad request"
                    })
                }
            })
            if (typeof body.number !== "string" ||
                !(/[A-Za-z0-9 ]+/.test(body.number))) {
                reject({
                    status: 400,
                    message: "Bad request"
                })
            } else {
                resolve();
            }

        })
    }

    const insertIntoDatabase = async (body, response) => {
        return db.doQuery("INSERT INTO rute (nume) VALUES (?)", [body.number])
            .then(async (result) => {
                ruta = result.insertId;
                var firstStation  = body.points[0];
                for (const item of body.points) {
                    var insertResult = await db.doQuery("INSERT INTO puncte (id_ruta, tip, coordonate_long, coordonate_lat, ordine) VALUES (?, ?, ?, ?, ?)", [result.insertId, item.type, item.coordinates[0], item.coordinates[1], item.order]);
                    if (item.order == 1) {
                        firstStation = insertResult.insertId;
                    }
                };
                return db.doQuery("INSERT INTO vehicule_transport (id_ruta, statie_noua) VALUES (?, ?)", [result.insertId, firstStation]).then(resultInsertVehicul => {
                    sendResponse(response, undefined, {
                        status: 200,
                        message: "Rută inserată",
                        qr: body.token,
                        vehicul: resultInsertVehicul.insertId,
                        ruta: result.insertId
                    });
                    return db.doQuery("INSERT INTO coduri_qr (id_vehicul, qr) VALUES (?, ?)", [resultInsertVehicul.insertId, body.token]);
                });
            })
            .catch(error => {
                var test = error;
                throw {
                    status: 500,
                    message: "Internal server error",
                }
            })
    }

    const sendResponse = (response, headers, body) => {
        writeResponse({
                header: {
                    status: 200,
                    reason: "OK",
                    headers: headers
                },
                body: JSON.stringify(body)
            },
            response
        );
    };

    const sendError = (response, error) => {
        var test = error;
        writeResponse({
                header: {
                    status: error.status
                },
                body: JSON.stringify({
                    error: true,
                    status: error.status,
                    message: "Ruta nu a putut fi adăugată"
                })
            },
            response
        );
    };

    return addMapRoute;
})();