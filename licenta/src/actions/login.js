module.exports = (() => {
    const db = require("../db/database");
    const genToken = require("../utils/genToken");
    const { setCookie } = require("../cookie");
    const { writeResponse } = require("../router");

    const getBody = request => {
        return new Promise((resolve, reject) => {
            let body = "";
            request.on("data", data => (body += data));
            request.on("end", () => resolve(body));
            request.on("error", error =>
                reject({
                    status: 500,
                    message: "Internal server error",
                    error: error
                })
            );
        });
    };

    const parseJSON = body => {
        return new Promise((resolve, reject) => {
            try {
                resolve(JSON.parse(body));
            } catch (error) {
                reject({
                    status: 500,
                    message: "Internal server error",
                    error: error
                });
            }
        });
    };

    const login = (request, response) => {
        getBody(request)
            .then(parseJSON)
            .then(body => {
                return db
                    .doQuery(
                        `SELECT * FROM utilizatori WHERE username=? and password=?`,
                        [body["username"], body["password"]]
                    )
                    .then(result => {
                        if (result.length === 1) {
                            const token = genToken(100);
                            const now = new Date();

                            return db
                                .doQuery(
                                    `INSERT INTO tokens (id_utilizator, token, time) VALUES (?, ?, ?)`,
                                    [
                                        result[0]["id"],
                                        token,
                                        `${now
                                            .toISOString()
                                            .slice(
                                                0,
                                                10
                                            )} ${now
                                            .toISOString()
                                            .slice(11, 19)}`
                                    ]
                                )
                                .then(() => {
                                    let redirectPage = "dashboard.page";
                                    if (result[0]["nivel_acces"] === 2) {
                                        redirectPage = "driverdashboard.page";
                                    }
                                    writeResponse(
                                        {
                                            header: {
                                                status: 200,
                                                reason: "OK",
                                                headers: setCookie(
                                                    "token",
                                                    token
                                                )
                                            },
                                            body: JSON.stringify({
                                                status: 200,
                                                response: "Logged in",
                                                goto: redirectPage
                                            })
                                        },
                                        response
                                    );
                                })
                                .catch(error => {
                                    throw {
                                        status: 500,
                                        message: "Internal server error",
                                        error: error
                                    };
                                });
                        } else {
                            throw {
                                status: 400,
                                message: "Bad request"
                            };
                        }
                    })
                    .catch(error => {
                        if (typeof error === "object") {
                            throw error;
                        } else {
                            throw {
                                status: 500,
                                message: "Internal server error",
                                error: error
                            };
                        }
                    });
            })
            .catch(error => {
                writeResponse(
                    {
                        header: {
                            status: error.status
                        },
                        body: JSON.stringify({
                            status: error.status,
                            error: error.message
                        })
                    },
                    response
                );
            });
    };

    return login;
})();
