module.exports = (() => {
    const db = require("../db/database");
    const { writeResponse } = require("../router");
    const { getBody, parseBody } = require("../helpers/helpers");
    const genToken = require("../utils/genToken");
	
    const insertIntoDatabase = body => {
        return db.doQuery('SELECT * FROM rute WHERE id = ?', [body.id]).then(ruta => {
            return db.doQuery('SELECT * FROM puncte WHERE id_ruta = ? order by ordine asc', [ruta[0].id]).then(id_puncte => {
                return db.doQuery('INSERT INTO vehicule_transport (id_ruta, statie_noua) VALUES (?, ?)', [ruta[0].id, id_puncte[0].id])
                .then(result => {
                    const qr = genToken(100);
                    return db.doQuery('INSERT INTO coduri_qr (id_vehicul, qr) VALUES (?, ?)', [result.insertId, qr]).then(qrInsert => {
                        return {
                            status: 200,
                            message: "Vehicul adăugat",
                            id: result.insertId,
                            qr: qr
                        };
                    })
                });
            });
        });
    };

    const addRouteVehicle = (request, response) => {
        getBody(request)
            .then(parseBody)
            .then(body => {
                return validateBody(body)
                    .then(insertIntoDatabase.bind(null, body))
                    .then(sendResponse.bind(null, response, undefined))
            })
            .catch(sendError.bind(null, response))
    }

    const validateBody = body => {
        return new Promise((resolve, reject) => {
            if (typeof body.id === "string" ||
                !(/[A-Za-z0-9 ]+/.test(body.id))) {
                resolve()
            } else {
                reject({
                    status: 500,
                    message: "Id nu este string"
                })
            }
        })
    }

    const sendResponse = (response, headers, body) => {
        writeResponse({
                header: {
                    status: 200,
                    reason: "OK",
                    headers: headers
                },
                body: JSON.stringify(body)
            },
            response
        );
    };

    const sendError = (response, error) => {
        writeResponse({
                header: {
                    status: error.status
                },
                body: JSON.stringify(error)
            },
            response
        );
    };

	return addRouteVehicle;
})();
