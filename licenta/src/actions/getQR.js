module.exports = (() => {
	const db = require("../db/database");

	const getQR = (req, res, parseData) => {
		db.doQuery(`SELECT * FROM coduri_qr WHERE id_vehicul=?`, [parseData.id]).then(qr => {
			res.writeHead(200, {"Content-Type": "application/json"});
			res.end(JSON.stringify(qr));
		});
	};
	
	return getQR;
})();

