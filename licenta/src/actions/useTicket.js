module.exports = (() => {
    const { getCookie } = require("../cookie");
    const http = require("http");
    const db = require("../db/database");
    const { getBody, parseBody } = require("../utils/requestUtils");
    const { writeResponse } = require("../router");

    const tablePath = "/table/v1/driving/";
    let options = {
        host: "0.0.0.0",
        port: 5000,
        path: "/route/v1/driving/"
    };

    const checkAndGetUserFlow = request => {
        const token = getCookie(request["headers"], "token");

        return new Promise((resolve, reject) => {
            db.doQuery(`SELECT id_utilizator FROM tokens WHERE token=?`, [
                token
            ]).then(tokens => {
                return db.doQuery(`SELECT * FROM utilizatori WHERE id=?`, [
                    tokens[0]["id_utilizator"]
                ]).then(users => {
                    if (users[0]["numar_bilete"] === 0 && users[0]["nivel_acces"] !== 2) {
                        reject({
                            status: 400,
                            message: "Utilizatorul nu mai are bilete.",
                            error: "User not found"
                        });
                    } else {
                        resolve(users[0]);
                    }
                })
                .catch(error => {
                    throw {
                        status: 500,
                        message: "Internal server error",
                        error: error
                    }
                })
            })
            .catch(error => {
                if (typeof error === "object") {
                    throw error
                }
                else {
                    throw {
                        status: 500,
                        message: "Internal server error",
                        error: error
                    }
                }
            })
        });
    };

    const getVehicleFlow = request => {
        return new Promise((resolve, reject) => {
            getBody(request)
                .then(parseBody)
                .then(body => {
                    db.doQuery(`SELECT * FROM coduri_qr WHERE qr=?`, [
                        body["qr"]
                    ]).then(result => {
                        if (result[0] === undefined) {
                            reject({
                                status: 404,
                                message: "Ruta nu a fost gasita"
                            });
                        } else {
                            resolve(result[0]);
                        }
                    });
                })
                .catch((error)=>{

                    if (typeof error === "object") {
                        reject(error)
                    }
                    else {
                        reject({
                            status: 500,
                            message: "Internal server error",
                            error: error
                        });
                    }
            })
        });
    };

    const updateVehicleLocation = (request, user, route) => {
        getBody(request)
            .then(parseBody)
            .then(body => {
                db.doQuery("SELECT * FROM vehicule_transport WHERE id = ? ", [route["id_vehicul"]])
                    .then(vehicul => {
                        db.doQuery("SELECT * FROM puncte WHERE id = ?", [vehicul[0].statie_noua])
                        .then(punct => {
                            db.doQuery("SELECT * FROM puncte WHERE ordine < ? and id_ruta = ?", [punct[0].ordine, punct[0].id_ruta])
                                .then(punctPrecedent => {
                                    db.doQuery("SELECT * FROM puncte WHERE ordine > ? and id_ruta = ?", [punct[0].ordine, punct[0].id_ruta])
                                        .then(punctViitor => {
                                            let path = tablePath;
                                            path += `${punct[0].coordonate_long},${punct[0].coordonate_lat};`;
                                            if (punctViitor.length > 0 ) {
                                                path += `${punctViitor[0].coordonate_long},${punctViitor[0].coordonate_lat};`;
                                            }
                                            if (punctPrecedent.length > 0) {
                                                path += `${punctPrecedent[0].coordonate_long},${punctPrecedent[0].coordonate_lat};`;
                                            }
                                            path = path.substr(0, path.length - 1) + '?sources=0';
                                            options.path = path;

                                            let data = "";

                                            const req = http.request(options, res => {
                                                res.on("data", chunk => {
                                                    data += chunk;
                                                });
                                                res.on("end", () => {
                                                    let parsedData = JSON.parse(data);

                                                    let punctId = punctViitor.length > 0 ? punctViitor[0].id : punctPrecedent[0].id;

                                                    if (punctViitor.length > 0 && punctPrecedent.length > 0) {
                                                        if (parsedData.durations[0][1] > parsedData.durations[0][2]) {
                                                            punctId = punctPrecedent[0].id;
                                                        } else {
                                                            punctId = punctViitor[0].id;
                                                        }
                                                    }

                                                    let currentDate = new Date();
                                                    var stringDate = currentDate.toISOString().slice(0, 19).replace('T', ' ');
                                                    db.doQuery("UPDATE vehicule_transport SET statie_noua = ?, data_update = ?", [punctId, stringDate]);
                                                });
                                                res.on("error", error => console.log(error));
                                            });

                                            req.end();
                                        });
                                });
                        });
                    });
                db.doQuery(`UPDATE rute SET coordonate_long=? , coordonate_lat=? where id=?`, [body.location.latitude, body.location.longitude, route["id_vehicul"]]);
            });
    };

    const userAndVehicleFlow = request => {
        return Promise.all([
            checkAndGetUserFlow(request),
            getVehicleFlow(request)
        ]);
    };

    const updateDriverVehicle = (user, route) => {
        return db
            .doQuery(
                `SELECT * FROM vehicule_transport WHERE id = ?`, [route["id_vehicul"]]
            ).then(resultSelectVehicule => {
                return db
                    .doQuery(`SELECT * FROM puncte WHERE id_ruta = ? order by ordine asc`, [resultSelectVehicule[0]["id_ruta"]]
                    ).then(resultSelectPuncte => {
                        var directieInainte = 1;
                        var statieNoua = resultSelectPuncte[0]["id"];
                        if (resultSelectVehicule[0]["directie_inainte"] == 1) {
                            var lastPos = resultSelectPuncte.length - 1;
                            statieNoua  = resultSelectPuncte[lastPos]["id"];
                            directieInainte = 0;
                        }
                        return db
                            .doQuery("UPDATE vehicule_transport SET statie_noua = ?, directie_inainte = ? WHERE id = ?", [statieNoua, directieInainte, route["id_vehicul"]]);
                    });
            }).catch(err => {
                var test = err;
                console.log(err);
            });
    }

    const addUsedTicket = (user, route) => {
        return db
            .doQuery(
                `INSERT INTO istoric_bilete (id_utilizator, id_ruta) VALUES (?, (SELECT id_ruta FROM vehicule_transport WHERE id = ?))`,
                [
                    user["id"],
                    route["id_vehicul"]
                ]
            )
            .then(() => {
                return db.doQuery(`UPDATE utilizatori SET numar_bilete=? WHERE id=?`, [
                    user["numar_bilete"] - 1,
                    user["id"]
                ]);
            });
    };

    const useTicket = (request, response) => {
        userAndVehicleFlow(request)
            .then(([user, route]) => {
                if (user["nivel_acces"] == 2) {
                    return updateDriverVehicle(user, route);
                } else {
                    updateVehicleLocation(request, user, route);
                    return addUsedTicket(user, route);
                }
            })
            .then(
                writeResponse.bind(
                    null,
                    {
                        header: {
                            status: 200
                        },
                        body: JSON.stringify({
                            response: "Ticket used"
                        })
                    },
                    response
                )
            )
            .catch(error => {
                writeResponse(
                    {
                        header: {
                            status: error.status
                        },
                        body: JSON.stringify({
                            status: error.status,
                            message: error.message
                        })
                    },
                    response
                );
            });
    };

    return useTicket;
})();

// TO TEST THE LOCATION AND QR ADDING IF NO CAMERA: PUT THIS IN THE WEB CONSOLE AND HIT ENTER
// fetch("/tickets/use", {
//     method: "POST",
//     credentials: "same-origin",
//     body: JSON.stringify({
//         qr:"test",
//         location: myLocation
//     })
// })
//     .then(result => result.json())
//     .then(result => {
//         console.log(result);
//
//         const ok = document.createElement("div")
//         scannerDiv.appendChild(ok);
//         setTimeout(() => {
//             scannerDiv.classList.add("msform-big-closing");
//             scannerDiv.classList.remove("msform-big");
//             setTimeout(() => {
//                 scannerDiv.remove();
//             }, 250)
//         }, 1000);
//     })
