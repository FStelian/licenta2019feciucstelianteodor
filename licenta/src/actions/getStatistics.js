
module.exports = (() => {
    const database = require("../db/database");
    const { writeResponse } = require("../router");

    const getStatistics = async (request, response) =>{
        var stats = {
          chart:{
              users:[],
              routes:[]
          },
            usersCount:"",
            adminsCount:""
        };
        await database.doQuery(`SELECT nume, count FROM rute r JOIN (SELECT id_ruta, count(id_ruta) as "count" FROM istoric_bilete i JOIN utilizatori u on i.id_utilizator = u.id group by i.id_ruta limit 10) c on r.id = c.id_ruta`)
            .then(results => {
                if (results.length >= 1) {
                    stats.chart.routes = results;
                } else {
                    throw new Error(error);
                }
            })
            .catch(error => {
                console.log(error);
            });
        await database.doQuery(`select id_utilizator, username, email, COUNT(id_utilizator) as "count" from istoric_bilete i JOIN utilizatori u on i.id_utilizator = u.id GROUP BY i.id_utilizator limit 10`)
            .then(results => {
                if (results.length >= 1) {
                    stats.chart.users = results;
                } else {
                    throw new Error(error);
                }
            })
            .catch(error => {
                console.log(error);
            });
        await database.doQuery(`SELECT count(*) as "count" FROM utilizatori WHERE nivel_acces = 0 `)
            .then(results => {
                if (results.length >= 1) {
                    stats.usersCount = results[0];
                } else {
                    throw new Error(error);
                }
            })
            .catch(error => {
                console.log(error);
            });
        database.doQuery(`SELECT count(*) as "count" FROM utilizatori WHERE nivel_acces = 1`)
            .then(results => {
                if (results.length >= 1) {
                    stats.adminsCount = results[0];
                } else {
                    throw new Error(error);
                }
                writeResponse(
                    {
                        header: {
                            status: 200,
                            reason: "OK"
                        },
                        body: JSON.stringify({
                            status: 200,
                            response: stats
                        })
                    }
                    ,response
                );
            })
            .catch(error => {
                console.log(error);
            });



    };
    return getStatistics;

})();

//example of object returned:
// {
//     "status": 200,
//     "response": {
//     "chart": {
//         "users": [
//             {
//                 "id_utilizator": 1,
//                 "username": "admin",
//                 "email": "AWTCproiect@gmail.com",
//                 "count": 6
//             },
//             {
//                 "id_utilizator": 2,
//                 "username": "user",
//                 "email": "user@userofAWTC.ro",
//                 "count": 5
//             }
//         ],
//             "routes": [
//             {
//                 "id_ruta": 28,
//                 "count": 5
//             },
//             {
//                 "id_ruta": 52,
//                 "count": 6
//             }
//         ]
//     },
//     "usersCount": {
//         "count": 1
//     },
//     "adminsCount": {
//         "count": 4
//     }
// }
// }
