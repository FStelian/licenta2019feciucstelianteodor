module.exports = (() => {
    const db = require("../db/database");
    const { getCookie } = require("../cookie");
    const { getBody, parseBody } = require("../helpers/helpers");
    const { writeResponse } = require("../router");

    const editProfile = (request, response) => {
        let { headers } = request;
        const token = getCookie(headers, "token");

        getBody(request)
            .then(parseBody)
            .then(body => {
                return validateBody(body)
                    .then(db.doQuery.bind(null, `SELECT * FROM tokens WHERE token=?`, [token]))
                    .then(users => {
                        if (users.length === 1) {
                            return db.doQuery(`UPDATE utilizatori SET nume=?, prenume=?, email=?, varsta=?, nr_telefon=? WHERE id=?`, [body.firstname, body.lastname, body.email, body.age, body.phone, users[0].id_utilizator])
                                .then(() => {
                                    return;
                                })
                                .catch(error => {
                                    throw {
                                        status: 500,
                                        message: "Internal server error"
                                    }
                                })
                        } else {
                            throw {
                                status: 400,
                                message: "Id utilizator inexistent"
                            }
                        }
                    })
                    .catch(error => {
                        if (typeof error === "object") {
                            throw error
                        }
                        else {
                            throw {
                                status: 500,
                                message: "Internal server error"
                            }
                        }
                    })
            })
            .then(() => {
                writeResponse({
                        header: {
                            status: 200,
                        },
                        body: JSON.stringify({
                            status: 200,
                            message: "Profilul a fost editat cu succes"
                        })
                    },
                    response
                )
            })
            .catch(error => {
                writeResponse({
                        header: {
                            status: error.status
                        },
                        body: JSON.stringify(error)
                    },
                    response
                );
            })
    }

    const validateBody = body => {
        return new Promise((resolve, reject) => {
            let errors = "";

            if (body.firstname === undefined || !/^[A-Za-z0-9-]+$/.test(body.firstname)) {
                errors += "Nume invalid\n";
            }
            if (body.lastname === undefined || !/^[A-Za-z0-9-]+$/.test(body.lastname)) {
                errors += "Prenume invalid\n";
            }
            if (body.email === undefined || !/^[A-Za-z0-9][A-Za-z0-9-_\.]*(@)[a-z0-9]+(\.)[a-z]+$/.test(body.email)) {
                errors += "E-mail invalid\n";
            }
            if (body.age === undefined || !/^[1-9][0-9]+$|^[1-9]$/.test(body.age)) {
                errors += "Vârstă invalidă\n";
            }
            if (body.phone === undefined || !/(02|07)[0-9]{8}/.test(body.phone)) {
                errors += "Număr de telefon invalid\n";
            }

            if (errors !== "") {
                reject({
                    status: 400,
                    message: errors
                })
            } else {
                resolve();
            }
        })
    }

    return editProfile;
})();

//example of object that has to be sent by frontend:
// token is taken from header, it finds the id of the user in the db and it is going to update it
// IMPORTANT! : If nothing is given, it is going to have BLANK fields.
// {
//     "username": "admin",
//     "password": "admin",
//     "nume" : "admi",
//     "prenume": "admi",
//     "varsta" : 20,
//     "email": "admin@gmail.com"
// }