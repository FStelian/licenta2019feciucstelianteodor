module.exports = (() => {
    const http = require("http");
    const { getBody, parseBody } = require("../helpers/helpers");
    const { writeResponse } = require("../router");

    const defaultPath = "/route/v1/driving/";
    let options = {
        host: "0.0.0.0",
        port: 5000,
        path: "/route/v1/driving/"
    };

    const getMapRoute = (request, response) => {
        getBody(request)
            .then(parseBody)
            .then(createURL)
            .then(requestURL)
            .then(([body, headers]) =>
                parseBody(body)
                .then(sendResponse.bind(null, response, headers))
            )
            .catch(sendError.bind(null, response));
    };

    const createURL = json => {
        let path = defaultPath;
        json.points.forEach(element => {
            const [lon, lat] = element.coordinates;
            path += `${lon},${lat};`;
        });
        for (let i = json.points.length - 2; i >= 0; i--) {
            const [lon, lat] = json.points[i].coordinates;
            path += `${lon},${lat};`;
        }
        path = path.substr(0, path.length - 1);
        options.path = path;
        // options.path = '/route/v1/driving/27.57980347,47.14841063;27.57942893,47.14110812;27.57306185,47.13422926;27.56956621,47.13057716;27.57381092,47.15265581';
        return options;
    };

    const requestURL = options => {
        return new Promise((resolve, reject) => {
            let data = "";

            const req = http.request(options, res => {
                res.on("data", chunk => {
                    data += chunk;
                });
                res.on("end", () => resolve([data, res.headers]));
                res.on("error", error => reject(error));
            });

            req.on("error", error => reject(error));
            req.end();
        });
    };

    const sendResponse = (response, headers, body) => {
        
        writeResponse(
            {
                header: {
                    status: 200,
                    reason: "OK",
                    headers: headers
                },
                body: JSON.stringify(body)
            },
            response
        );
    };

    const sendError = (response, error) => {
        writeResponse(
            {
                header: {
                    status: 500
                },
                body: error
            },
            response
        );
    };

    return getMapRoute;
})();
