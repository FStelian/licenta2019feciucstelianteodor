module.exports = (() => {
    const db = require("../db/database");
    const {getCookie} = require("../cookie");
    const {writeResponse} = require("../router");

    const changePassword = (request, response) => {

        let {headers} = request;
        const token = getCookie(headers, "token");

        let body = "";
        let id;
        let password = {
            newPassword: "",
            oldPassword: ""
        };
        request.on("data", data => (body += data));
        request.on("end", () => {
            body = JSON.parse(body);
            password.oldPassword = body ["oldPassword"];
            password.newPassword = body ["newPassword"];

            db.doQuery(`SELECT * FROM tokens WHERE token=?`, [token]
            ).then(result => {
                if (result.length === 1) {
                    id = result[0]["id_utilizator"];
                    return db
                        .doQuery(`UPDATE utilizatori SET password = ? WHERE id= ?`,
                            [password.newPassword, id]
                        ).then(() => {
                            writeResponse(
                                {
                                    header: {
                                        status: 200,
                                        reason: "OK",
                                        //  headers: setCookie("token", token)
                                    },
                                    body: JSON.stringify({
                                        status: 200,
                                        response: "Password changed"
                                    })
                                },
                                response
                            );
                        })
                }
                else {
                    writeResponse(
                        {
                            header: {
                                status: 400,
                                reason: "Bad request"
                            },
                            body: JSON.stringify({
                                status: 400,
                                error: "Could not change password"
                            })
                        },
                        response
                    );

                }
            }).catch(error => {
                writeResponse(
                    {
                        header: {
                            status: 500,
                            reason: "Internal server error"
                        },
                        body: JSON.stringify({
                            status: 500,
                            error: "Internal server error"
                        })
                    },
                    response
                );
            })
        });
    }


                return changePassword;
    }) ();

//example of payload
//
// {
//     "oldPassword" : "admin",
//     "newPassword" : "admin"
// }