-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 02, 2019 at 02:39 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `awtc`
--

-- --------------------------------------------------------

--
-- Table structure for table `bilete`
--

CREATE TABLE `bilete` (
  `id` int(20) NOT NULL,
  `pret` int(6) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coduri_qr`
--

CREATE TABLE `coduri_qr` (
  `id` int(20) NOT NULL,
  `id_vehicul` int(20) NOT NULL,
  `qr` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inbox`
--

CREATE TABLE `inbox` (
  `id` int(20) UNSIGNED NOT NULL,
  `subject` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `message` varchar(10000) COLLATE latin1_general_ci NOT NULL,
  `timestamp` date NOT NULL,
  `id_utilizator` int(20) NOT NULL,
  `email` varchar(255) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;


-- --------------------------------------------------------

--
-- Table structure for table `istoric_bilete`
--

CREATE TABLE `istoric_bilete` (
  `id` int(20) NOT NULL,
  `id_utilizator` int(20) NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_ruta` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `puncte`
--

CREATE TABLE `puncte` (
  `id` int(20) NOT NULL,
  `id_ruta` int(20) NOT NULL,
  `tip` varchar(20) NOT NULL,
  `coordonate_lat` decimal(10,8) NOT NULL,
  `coordonate_long` decimal(10,8) NOT NULL,
  `ordine` int(6) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `rute`
--

CREATE TABLE `rute` (
  `id` int(20) NOT NULL,
  `nume` varchar(25) NOT NULL,
  `data_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `tokens`
--

CREATE TABLE `tokens` (
  `id_utilizator` int(20) NOT NULL,
  `token` varchar(500) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `tranzactii_deschise`
--

CREATE TABLE `tranzactii_deschise` (
  `id` varchar(100) NOT NULL,
  `id_utilizator` int(20) DEFAULT NULL,
  `numar_bilete` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `utilizatori`
--

CREATE TABLE `utilizatori` (
  `id` int(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `nume` varchar(20) NOT NULL,
  `prenume` varchar(20) NOT NULL,
  `varsta` int(3) NOT NULL,
  `nivel_acces` int(1) DEFAULT '0',
  `numar_bilete` int(5) DEFAULT NULL,
  `id_bilet` int(20) NOT NULL,
  `nr_telefon` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `vehicule_transport`
--

CREATE TABLE `vehicule_transport` (
  `id` int(20) NOT NULL,
  `id_ruta` int(20) NOT NULL,
  `statie_noua` int(20) DEFAULT NULL,
  `directie_inainte` tinyint(1) DEFAULT '1',
  `data_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Indexes for table `bilete`
--
ALTER TABLE `bilete`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coduri_qr`
--
ALTER TABLE `coduri_qr`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `qr` (`qr`),
  ADD KEY `id_vehicul` (`id_vehicul`);

--
-- Indexes for table `inbox`
--
ALTER TABLE `inbox`
  ADD PRIMARY KEY (`id`),
  ADD KEY `inbox_fk_utilizatori` (`id_utilizator`);

--
-- Indexes for table `istoric_bilete`
--
ALTER TABLE `istoric_bilete`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_istoric_bilete_utilizatori` (`id_utilizator`),
  ADD KEY `fk_ruta` (`id_ruta`);

--
-- Indexes for table `puncte`
--
ALTER TABLE `puncte`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_ruta` (`id_ruta`);

--
-- Indexes for table `rute`
--
ALTER TABLE `rute`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_vehicul` (`nume`);

--
-- Indexes for table `tokens`
--
ALTER TABLE `tokens`
  ADD PRIMARY KEY (`token`),
  ADD KEY `tokens_fk` (`id_utilizator`);

--
-- Indexes for table `tranzactii_deschise`
--
ALTER TABLE `tranzactii_deschise`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_utilizator` (`id_utilizator`);

--
-- Indexes for table `utilizatori`
--
ALTER TABLE `utilizatori`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `fk_utilizatori_fk` (`id_bilet`);

--
-- Indexes for table `vehicule_transport`
--
ALTER TABLE `vehicule_transport`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_ruta` (`id_ruta`),
  ADD KEY `fk_statie_noua` (`statie_noua`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bilete`
--
ALTER TABLE `bilete`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `coduri_qr`
--
ALTER TABLE `coduri_qr`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `inbox`
--
ALTER TABLE `inbox`
  MODIFY `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `istoric_bilete`
--
ALTER TABLE `istoric_bilete`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `puncte`
--
ALTER TABLE `puncte`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=368;

--
-- AUTO_INCREMENT for table `rute`
--
ALTER TABLE `rute`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `utilizatori`
--
ALTER TABLE `utilizatori`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `vehicule_transport`
--
ALTER TABLE `vehicule_transport`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `coduri_qr`
--
ALTER TABLE `coduri_qr`
  ADD CONSTRAINT `fk_id_vehicul` FOREIGN KEY (`id_vehicul`) REFERENCES `vehicule_transport` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `inbox`
--
ALTER TABLE `inbox`
  ADD CONSTRAINT `inbox_fk_utilizatori` FOREIGN KEY (`id_utilizator`) REFERENCES `utilizatori` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `istoric_bilete`
--
ALTER TABLE `istoric_bilete`
  ADD CONSTRAINT `fk_ruta` FOREIGN KEY (`id_ruta`) REFERENCES `rute` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_utilizatori` FOREIGN KEY (`id_utilizator`) REFERENCES `utilizatori` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `puncte`
--
ALTER TABLE `puncte`
  ADD CONSTRAINT `fk_id_ruta` FOREIGN KEY (`id_ruta`) REFERENCES `rute` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tokens`
--
ALTER TABLE `tokens`
  ADD CONSTRAINT `tokens_fkl` FOREIGN KEY (`id_utilizator`) REFERENCES `utilizatori` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tranzactii_deschise`
--
ALTER TABLE `tranzactii_deschise`
  ADD CONSTRAINT `fk_id_utilizator` FOREIGN KEY (`id_utilizator`) REFERENCES `utilizatori` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `utilizatori`
--
ALTER TABLE `utilizatori`
  ADD CONSTRAINT `fk_id_bilet` FOREIGN KEY (`id_bilet`) REFERENCES `bilete` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `vehicule_transport`
--
ALTER TABLE `vehicule_transport`
  ADD CONSTRAINT `fk_statie_noua` FOREIGN KEY (`statie_noua`) REFERENCES `puncte` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `id_ruta` FOREIGN KEY (`id_ruta`) REFERENCES `rute` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
