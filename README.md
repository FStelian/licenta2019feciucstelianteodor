# AWTC

AWTC

The current software is under the GPLv3 License. A description of the License can pe found in the LICENSE.md file.

Pentru a folosi serviciul OSRM-Backedn se recomada utilizarea imaginii docker gasita la aceasta adresa si urmarea instructiunilor: https://hub.docker.com/r/osrm/osrm-backend/

O scurta descriere a proiectului:

Aceasta este  o aplicatie web bazata pe servicii (API in stilul REST) care faciliteaza folosirea transportului in comun. 
Fiecare utilizator se va inregistra, isi va putea achizitiona intr-un mod securizat bilete folosind un API de payment (ex: Paypal),
 si va putea verifica statusul contului, numarul de bilete nefolosite, istoria biletelor folosite (ruta, data), si informatii despre 
 mijloacele de transport (unde se afla acestea, in cat timp ajung intr-o statie etc.). 


Folosirea unui bilet se va face scanand de catre utilizator in mijlocul de transport un cod QR care va fi unic per ruta. 
La scanarea acelui cod QR, se va folosi unul din bilete existente ale utilizatorului. 
Utilizatorii vor putea verifica in timp real momentul in care un mijloc de transport va ajunge intr-o statie. 
Pentru a putea aproxima cu o eroare cat mai mica cand un mijloc de transport va ajunge intr-o statie, 
la compostarea unui bilet folosind codul QR de catre un utilizator, aplicatia va cere permisiunea folosirii locatiei si preluarii datelor GPS. 
Calculand distanta dintre locatia utlizatorului in momentul scanarii codului QR si urmatoarea statie a mijlocului de transport, 
se poate aproxima durata de timp pana la ajungerea in urmatoarea statie. 
Se va implementa un mod prin care utilizatorii cu statusul de administrator vor putea vizualiza datele stocate despre utilizatori/calatorii si 
bilete intr-o maniera atractiva, si vor putea adauga/scoate rute ale mijloacelor de transport si pot vedea codurile QR ale acestora. 
La adaugarea unui nou mijloc de transport in modul administrator se va genera un nou cod QR ce va apartine acestei rute, 
si va putea fi folosit imediat dupa generare de catre utilizatori.